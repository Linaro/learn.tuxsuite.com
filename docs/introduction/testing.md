# Testing

Submit a test request using the tuxsuite command line interface. This will
wait for the test to complete before returning by default.

```shell
tuxsuite test \
    --device qemu-arm64 \
    --kernel https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/Image.gz \
    --modules https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/modules.tar.xz
```

The output will look like:

```shell
Testing https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/Image.gz on qemu-arm64 with boot
uid: 1uFgOe2H7UStAvEznAvAL77Qts5
⚙️ Provisioning: [boot] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5
🚀 Running: [boot] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5
🎉 Pass: [boot] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5
```

The results
([logs](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5/logs?format=html),
[results](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5/results),
...) will be available at
[tuxapi.tuxsuite.com](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5)
under a unique and non-guessable URL.


!!! tip "testing"
    Go to [testing](../../cli/testing/single) for more information about the tests.
