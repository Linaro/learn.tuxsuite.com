# Installing TuxSuite CLI

## Using pip

You could install TuxSuite CLI using pip with:

``` shell
python3 -m pip install --upgrade tuxsuite
```

## Using docker

tuxsuite is also available as a docker container at
[tuxsuite/tuxsuite](https://hub.docker.com/r/tuxsuite/tuxsuite).

For example, to run tuxsuite via docker:

```
docker run tuxsuite/tuxsuite tuxsuite build --help
```


