# Building

## The linux Kernel

Submit a build request using the tuxsuite command line interface. This will
wait for the build to complete before returning by default.

```shell
tuxsuite build \
    --git-repo "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git" \
    --git-ref "master" \
    --target-arch "arm64" \
    --kconfig "defconfig" \
    --toolchain "gcc-9"
```

The output will look like:

```shell
Building Linux Kernel https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git at master
uid: 1u7Du0Ut6g0qKSd44DMcGeAKsKl
⚙️ Provisioning: arm64 (defconfig) with gcc-9 @ https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") arm64 (defconfig) with gcc-9 @ https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") arm64 (defconfig) with gcc-9 @ https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/
👾 Pass (1 warning): fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") arm64 (defconfig) with gcc-9 @ https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/
```

The results
([kernel](https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/vmlinux.xz),
[modules](https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/modules.tar.xz),
[headers](https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/headers.tar.xz),
[logs](https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/build.log), ...)
will be available at
[builds.tuxbuild.com](https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/)
under a unique and non-guessable URL.


!!! tip "building"
   Go to [building](../../cli/building/single) for more information about the builds.


## OpenEmbedded/Yocto
   Go to [OpenEmbedded](../../cli/buildingoe/single) for more information about the builds.

## Mbed-TLS
   Go to [Mbed-TLS](../../cli/buildingmbed/single) for more information about the builds.

## Building from Private Repository
   Go to [Private Builds](../../howtos/private_repositories) for more information about Private builds.
