# Configuring

## Account request

In order to use TuxSuite, you first need an account.

To request access, email us at
[tuxsuite@linaro.org](mailto:tuxsuite@linaro.org) or fill out our
[access request form](https://forms.gle/3NaW5fuNykGstsMq6).

## Token management

You can mange your authentication tokens on [tuxsuite.com](https://tuxsuite.com/tokens).

## TuxSuite configuration

The Authentication token needs to be stored in `~/.config/tuxsuite/config.ini`.
The minimal format of the ini file is given below:

``` ini
$ cat ~/.config/tuxsuite/config.ini
[default]
token=vXXXXXXXYYYYYYYYYZZZZZZZZZZZZZZZZZZZg
```

Alternatively, the `TUXSUITE_TOKEN` environment variable may be provided.
