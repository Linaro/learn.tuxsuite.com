# Building android userspace

## Introduction

[Tuxsuite](https://docs.tuxsuite.com) already has the support for building android kernel builds. It has now extended support for building and testing android userspace on a real hardware as well using [LAVA](https://docs.lavasoftware.org/lava/index.html).
Below is the required prerequisites and configuration that is needed to do android userspace build using Tuxsuite.

## Prerequisites

* Tuxsuite
* build-definition.json ( single build)
* plan (multiple build)

## Build definition

Build-definition for building aosp is similar to what we already have for android
kernel build except with some extra fields that needs to be set and passed in build-definition.json file.

* lunch_target
* build_type (`user`, `userdebug`, `eng`)


=== "**Android Definition**"
    - `container`: The container used by Docker to do the build.
    - `name`: (optional) Name of your choice.
    - `sources`:
        - `android`: Android repo source, its branch, manifest, build-config and its url to be provided under `android` field.
            - `branch`: (string)
            - `manifest`: (string)
            - `url`: (string)
            - `build_config`: (optional, string)
            - `build_type`: (string) `user` as default. can have values `user`, `userdebug` and `eng`.
            - `bazel`: (bool) `false` as default
            - `lunch_target`: (string)
            - `group`: (string) `default` as default
            - `patchset_script`: (string)
            - `patchset_project`: (string)


### Sample Android userspace build definition

```json
{
    "artifacts": [],
    "bblayers_conf": [],
    "container": "ubuntu-20.04",
    "distro": null,
    "environment": {},
    "envsetup": null,
    "local_conf": [],
    "machine": null,
    "name": "",
    "sources": {
        "android": {
            "branch": "main",
            "manifest": "default.xml",
            "url": "https://android.googlesource.com/platform/manifest",
            "lunch_target": "db845c",
            "build_type": "user"
        }
    },
    "targets": null
}

```

!!! Tip "Note"
    To build android userspace, field `lunch_target` should be passed in sources of build-definition file. It accepts the lunch target string. Also,
    the `build_type` should be passed which accepts values such as `user`, `userdebug` and `eng` as explained in <https://source.android.com/docs/setup/build/building>


## Testing AOSP

To test an aosp image using [real hardware](https://docs.tuxsuite.com/tuxtest/real-hardware-testing/), submit the request using the tuxsuite command line interface.

```shell

tuxsuite test submit --lab https://validation.linaro.org  --lava-test-plans-project lkft-android --device dragonboard-845c --parameters BUILD_REFERENCE_IMAGE_GZ_URL=https://storage.tuxsuite.com/public/demo/demo/oebuilds/2V0nEaGwAaJl0H127YvUm4wmovB/Image.gz --parameters TUXSUITE_BAKE_VENDOR_DOWNLOAD_URL=https://storage.tuxsuite.com/public/demo/demo/oebuilds/2V0nQ5Y1ikd82Z2dMMaYxQcHOW1/ --parameters LKFT_BUILD_CONFIG=lkft-d
b845c-aosp-master-mainline-gki --parameters IMAGE_SUPPORTED_VENDOR_BOOT=true

```

Above command will boot test dragonboard-845c device with provided general kernel image (`BUILD_REFERENCE_IMAGE_GZ_URL`) and vendor image (`TUXSUITE_BAKE_VENDOR_DOWNLOAD_URL`).

**Note**: Variables passed in `--parameters` option is referenced from [lava-test-plan](https://github.com/Linaro/lava-test-plans) repository and from the respective lava test plan projects i.e `lkft-android` in this case.

### Example plan

```yaml
version: 1
name: dragonboard-845c build and boot test.
description: Demonstrate a build and boot test with dragonboard-845c via real hardware
jobs:
- name: android
  bake:
    - artifacts: []
      container: ubuntu-20.04
      name: ''
      sources:
        android:
          bazel: true
          branch: common-android-mainline
          build_config: "//common:db845c_dist"
          manifest: default.xml
          url: https://android.googlesource.com/kernel/manifest

  tests:
    - device: dragonboard-845c
      parameters: {BUILD_REFERENCE_IMAGE_GZ_URL: "https://storage.tuxsuite.com/public/linaro/lkft-android/oebuilds/2UUyDDQ9DLoebjDu1wTMZxkbb8G/Image.gz", TUXSUITE_BAKE_VENDOR_DOWNLOAD_URL: "$BUILD/", LKFT_BUILD_CONFIG: "lkft-db845c-aosp-master-mainline-gki", IMAGE_SUPPORTED_VENDOR_BOOT: True}
```

!!! note
    Above plan has an android aosp build with a dependent test. We must have an already available general kernel image(gki) or vendor image URL to be passed in the plan file. Also if only download url is needed in a parameter, then it has to be passed as `$BUILD/`

### Supported Tests

* boot
* CTS/VTS

The testcases found in [ltp-testcases](https://github.com/Linaro/lava-test-plans/tree/master/lava_test_plans/testcases) can be referred with its name
and passed in `--tests` option in cli command for testing.