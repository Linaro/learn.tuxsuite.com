# Example

## Android Testing Plan Examples

!!! info "Android Boot Testing Plan"
    Android plan file containing single bake build and a boot test on dragonboard-845c and qrb5165-rb5

``` yaml
common_parameters: &common_parameters
  REFERENCE_BUILD_URL: http://testdata.linaro.org/lkft/aosp-stable/tuxsuite/lkft-aosp-main-db845c/1493198419
  BUILD_REFERENCE_IMAGE_GZ_URL: "$BUILD/Image.gz"
  TUXSUITE_BAKE_VENDOR_DOWNLOAD_URL: "$BUILD/"
  LKFT_BUILD_CONFIG: "lkft-db845c-aosp-master-mainline-gki-tuxtrigger"
  ANDROID_VERSION: "aosp-main"
  LAVA_JOB_PRIORITY: "high"
  IMAGE_SUPPORTED_VENDOR_BOOT: "true"


version: 1
name: Android Kernel Building for android-mainline
description: Building kernels for the Android and testing them
jobs:
- name: Build GKI and db845c on android-mainline
  bakes:
    - container: tuxbake/ubuntu-20.04
      sources:
        android:
          url: https://android.googlesource.com/kernel/manifest
          bazel: true
          branch: common-android-mainline
          build_config: //common:db845c_dist
          manifest: default.xml

  tests:
    - device: dragonboard-845c
      tests: []
      lava_test_plans_project: lkft-android
      lab: https://lkft.validation.linaro.org
      parameters:
        <<: *common_parameters

    - device: qrb5165-rb5
      tests: []
      lava_test_plans_project: lkft-android
      lab: https://lkft.validation.linaro.org
      parameters:
        <<: *common_parameters
```

!!! note "Note"
    To do a boot test, there is no need to specify it explicitly in `tests` as boot test is added by default in tuxsuite



!!! info "CTS Test Plan"
    Android plan file containing single bake build with CTS test on dragonboard-845c and qrb5165-rb5

``` yaml
common_parameters: &common_parameters
  REFERENCE_BUILD_URL: http://testdata.linaro.org/lkft/aosp-stable/tuxsuite/lkft-aosp-main-db845c/1493198419
  BUILD_REFERENCE_IMAGE_GZ_URL: "$BUILD/Image.gz"
  TUXSUITE_BAKE_VENDOR_DOWNLOAD_URL: "$BUILD/"
  LKFT_BUILD_CONFIG: "lkft-db845c-aosp-master-mainline-gki-tuxtrigger"
  ANDROID_VERSION: "aosp-main"
  LAVA_JOB_PRIORITY: "high"
  IMAGE_SUPPORTED_VENDOR_BOOT: "true"

common_cts_parameters: &common_cts_parameters
  TEST_CTS_URL: "http://testdata.linaro.org/lkft/aosp-stable/tuxsuite/lkft-aosp-main-cts/1493198419"
  TEST_CTS_VERSION: "lkft-aosp-main-cts/1493198419"
  xts_test_params: "cts --include-filter CtsLibcoreTestCases --disable-reboot"


version: 1
name: Android Kernel Building for android-mainline
description: Building kernels for the Android and testing them
jobs:
- name: Build GKI and db845c on android-mainline
  bakes:
    - container: tuxbake/ubuntu-20.04
      sources:
        android:
          url: https://android.googlesource.com/kernel/manifest
          bazel: true
          branch: common-android-mainline
          build_config: //common:db845c_dist
          manifest: default.xml

  tests:
    - device: dragonboard-845c
      tests: [android-cts]
      lava_test_plans_project: lkft-android
      lab: https://lkft.validation.linaro.org
      timeouts: {"android-cts": 500}
      parameters:
        <<: *common_parameters
        <<: *common_cts_parameters

    - device: qrb5165-rb5
      tests: [android-cts]
      lava_test_plans_project: lkft-android
      lab: https://lkft.validation.linaro.org
      timeouts: {"android-cts": 500}
      parameters:
        <<: *common_parameters
        <<: *common_cts_parameters
```

!!! info "VTS Test Plan"
    Android plan file containing single bake build with VTS tests on dragonboard-845c and qrb5165-rb5

``` yaml
common_parameters: &common_parameters
  REFERENCE_BUILD_URL: http://testdata.linaro.org/lkft/aosp-stable/tuxsuite/lkft-aosp-main-db845c/1493198419
  BUILD_REFERENCE_IMAGE_GZ_URL: "$BUILD/Image.gz"
  TUXSUITE_BAKE_VENDOR_DOWNLOAD_URL: "$BUILD/"
  LKFT_BUILD_CONFIG: "lkft-db845c-aosp-master-mainline-gki-tuxtrigger"
  ANDROID_VERSION: "aosp-main"
  LAVA_JOB_PRIORITY: "high"
  IMAGE_SUPPORTED_VENDOR_BOOT: "true"

common_vts_parameters: &common_vts_parameters
  TEST_VTS_URL: "http://testdata.linaro.org/lkft/aosp-stable/tuxsuite/lkft-aosp-main-vts/1493198419"
  TEST_VTS_VERSION: "lkft-aosp-main-vts/1493198419"

version: 1
name: Android Kernel Building for android-mainline
description: Building kernels for the Android and testing them
jobs:
- name: Build GKI and db845c on android-mainline
  bakes:
    - container: tuxbake/ubuntu-20.04
      sources:
        android:
          url: https://android.googlesource.com/kernel/manifest
          bazel: true
          branch: common-android-mainline
          build_config: //common:db845c_dist
          manifest: default.xml

  tests:
    - device: dragonboard-845c
      tests: [android-vts-kernel-v7a]
      lava_test_plans_project: lkft-android
      lab: https://lkft.validation.linaro.org
      timeouts: {"android-vts-kernel-v7a": 500}
      parameters:
        <<: *common_parameters
        <<: *common_vts_parameters

    - device: dragonboard-845c
      tests: [android-vts-kernel-v8a]
      lava_test_plans_project: lkft-android
      lab: https://lkft.validation.linaro.org
      timeouts: {"android-vts-kernel-v8a": 500}
      parameters:
        <<: *common_parameters
        <<: *common_vts_parameters

    - device: qrb5165-rb5
      tests: [android-vts-kernel-v7a]
      lava_test_plans_project: lkft-android
      lab: https://lkft.validation.linaro.org
      timeouts: {"android-vts-kernel-v7a": 500}
      parameters:
        <<: *common_parameters
        <<: *common_vts_parameters

    - device: qrb5165-rb5
      tests: [android-vts-kernel-v8a]
      lava_test_plans_project: lkft-android
      lab: https://lkft.validation.linaro.org
      timeouts: {"android-vts-kernel-v8a": 500}
      parameters:
        <<: *common_parameters
        <<: *common_vts_parameters
```
