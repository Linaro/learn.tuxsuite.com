# Reproducing tests

For every test triggered using tuxsuite, you can reproduce the same test run locally.

TuxTest is using [TuxRun](https://tuxrun.org) for testing the linux kernel on
both cloud or local host. In order to use the exact same environment, TuxRun
is leveraging [podman](https://podman.io/) with custom container images.

## Setting up

### Host dependencies

You should install `podman` and `python`:

```shell
sudo apt install podman
sudo apt install python3 python3-pip
```

### TuxRun

You should now install TuxRun from [PyPi](https://pypi.org/project/tuxrun/):

```shell
python3 -m pip install --upgrade tuxrun
```

## Reproducing

To reproduce
[1yDfQuBu7L5Z662qFRla2uuypuu](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1yDfQuBu7L5Z662qFRla2uuypuu),
go to the web page and download the
[tuxrun_reproducer.sh](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1yDfQuBu7L5Z662qFRla2uuypuu/reproducer).

```shell
#!/bin/sh

# TuxRun is a command line tool and Python library that provides a way to
# boot and test linux kernels.
#
# TuxRun supports the concept of runtimes. For that to work it requires that
# you install podman or docker on your system.
#
# To install tuxrun on your system globally:
# sudo pip3 install -U tuxrun
#
# See https://tuxrun.org/ for complete documentation.

tuxrun --runtime podman \
       --device qemu-armv5 \
       --kernel https://storage.tuxboot.com/armv5/zImage \
       --image docker.io/lavasoftware/lava-dispatcher:2021.08
       --tests ltp-smoke ltp-fcntl-locktests
```

The output will look like:
```
[...]
2021-09-21T07:26:51 Boot command: /usr/bin/qemu-system-arm -cpu arm926 -machine versatilepb -nographic -nic none -m 256M -monitor none -kernel /var/lib/lava/dispatcher/tmp/1/deployimages-n5vn5l9o/kernel/zImage -append "console=ttyAMA0,115200 root=/dev/sda debug verbose console_msg_format=syslog" -dtb /var/lib/lava/dispatcher/tmp/1/deployimages-n5vn5l9o/dtb/versatile-pb.dtb -drive file=/var/lib/lava/dispatcher/tmp/1/deployimages-n5vn5l9o/rootfs/https___storage.tuxboot.com_armv5_rootfs.ext4,if=scsi,format=raw -drive format=qcow2,file=/var/lib/lava/dispatcher/tmp/1/apply-overlay-guest-6gjkn6xj/lava-guest.qcow2,media=disk,if=scsi,id=lavatest
2021-09-21T07:26:52 started a shell command
2021-09-21T07:26:52 end: 2.1.1 execute-qemu (duration 00:00:01) [common]
2021-09-21T07:26:52 {'case': 'execute-qemu', 'definition': 'lava', 'duration': '0.55', 'extra': {'host_arch': 'amd64', 'job_arch': 'arm', 'qemu_version': '1:5.2+dfsg-11'}, 'level': '2.1.1', 'namespace': 'common', 'result': 'pass'}
2021-09-21T07:26:52 end: 2.1 boot-qemu-image (duration 00:00:01) [common]
2021-09-21T07:26:52 start: 2.2 auto-login-action (timeout 00:02:59) [common]
2021-09-21T07:26:52 Setting prompt string to ['Linux version [0-9]']
2021-09-21T07:26:52 auto-login-action: Wait for prompt ['Linux version [0-9]'] (timeout 00:03:00)
2021-09-21T07:26:52 ALSA lib confmisc.c:767:(parse_card) cannot find card '0'
2021-09-21T07:26:52 ALSA lib conf.c:4745:(_snd_config_evaluate) function snd_func_card_driver returned error: No such file or directory
2021-09-21T07:26:52 ALSA lib confmisc.c:392:(snd_func_concat) error evaluating strings
2021-09-21T07:26:52 ALSA lib conf.c:4745:(_snd_config_evaluate) function snd_func_concat returned error: No such file or directory
2021-09-21T07:26:52 ALSA lib confmisc.c:1246:(snd_func_refer) error evaluating name
2021-09-21T07:26:52 ALSA lib conf.c:4745:(_snd_config_evaluate) function snd_func_refer returned error: No such file or directory
2021-09-21T07:26:52 ALSA lib conf.c:5233:(snd_config_expand) Evaluate error: No such file or directory
2021-09-21T07:26:52 ALSA lib pcm.c:2660:(snd_pcm_open_noupdate) Unknown PCM default
2021-09-21T07:26:52 alsa: Could not initialize DAC
2021-09-21T07:26:52 alsa: Failed to open `default':
2021-09-21T07:26:52 alsa: Reason: No such file or directory
2021-09-21T07:26:52 ALSA lib confmisc.c:767:(parse_card) cannot find card '0'
2021-09-21T07:26:52 ALSA lib conf.c:4745:(_snd_config_evaluate) function snd_func_card_driver returned error: No such file or directory
2021-09-21T07:26:52 ALSA lib confmisc.c:392:(snd_func_concat) error evaluating strings
2021-09-21T07:26:52 ALSA lib conf.c:4745:(_snd_config_evaluate) function snd_func_concat returned error: No such file or directory
2021-09-21T07:26:52 ALSA lib confmisc.c:1246:(snd_func_refer) error evaluating name
2021-09-21T07:26:52 ALSA lib conf.c:4745:(_snd_config_evaluate) function snd_func_refer returned error: No such file or directory
2021-09-21T07:26:52 ALSA lib conf.c:5233:(snd_config_expand) Evaluate error: No such file or directory
2021-09-21T07:26:52 ALSA lib pcm.c:2660:(snd_pcm_open_noupdate) Unknown PCM default
2021-09-21T07:26:52 alsa: Could not initialize DAC
2021-09-21T07:26:52 alsa: Failed to open `default':
2021-09-21T07:26:52 alsa: Reason: No such file or directory
2021-09-21T07:26:52 audio: Failed to create voice `lm4549.out'
2021-09-21T07:26:52 vpb_sic_write: Bad register offset 0x2c
2021-09-21T07:26:53 <6>Booting Linux on physical CPU 0x0
2021-09-21T07:26:53 <5>Linux version 5.10.7 (ivoire@draupnir) (arm-buildroot-linux-uclibcgnueabi-gcc.br_real (Buildroot -gb38dc87-dirty) 9.3.0, GNU ld (GNU Binutils) 2.35.2) #1 Thu Apr 29 14:44:13 CEST 2021
2021-09-21T07:26:53 <6>CPU: ARM926EJ-S [41069265] revision 5 (ARMv5TEJ), cr=00093177
2021-09-21T07:26:53 <6>CPU: VIVT data cache, VIVT instruction cache
2021-09-21T07:26:53 <6>OF: fdt: Machine model: ARM Versatile PB
2021-09-21T07:26:53 <6>Memory policy: Data cache writeback
2021-09-21T07:26:53 <6>Zone ranges:
2021-09-21T07:26:53 <6>  Normal   [mem 0x0000000000000000-0x000000000fffffff]
2021-09-21T07:26:53 <6>Movable zone start for each node
2021-09-21T07:26:53 <6>Early memory node ranges
2021-09-21T07:26:53 <6>  node   0: [mem 0x0000000000000000-0x000000000fffffff]
2021-09-21T07:26:53 <6>Initmem setup node 0 [mem 0x0000000000000000-0x000000000fffffff]
2021-09-21T07:26:53 <7>On node 0 totalpages: 65536
2021-09-21T07:26:53 <7>  Normal zone: 512 pages used for memmap
2021-09-21T07:26:53 <7>  Normal zone: 0 pages reserved
2021-09-21T07:26:53 <7>  Normal zone: 65536 pages, LIFO batch:15
2021-09-21T07:26:53 <7>pcpu-alloc: s0 r0 d32768 u32768 alloc=1*32768
[...]
```
