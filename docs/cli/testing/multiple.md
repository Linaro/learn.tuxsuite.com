# Multiple tests

Using the TuxSuite command line you can run multiple tests in parallel, making
use of the scalability of TuxSuite.

!!! tip "tuxsuite plan"
    You can also chain build and test with tuxsuite plan. For each, build a
    set of tests can be automatically ran. See [tuxsuite plan](../plan/kernel/arch.md).

## Multiple test suites

Using the given plan configuration file, you will test the same kernel with multiple test suites.

```yaml
version: 1
name: kernel validation
description: Build and test linux kernel variants
jobs:
- name: armv5
  tests:
    - {kernel: https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/Image.gz, device: qemu-arm64, tests: [ltp-fcntl-locktests]}
    - {kernel: https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/Image.gz, device: qemu-arm64, tests: [ltp-fs_bind]}
    - {kernel: https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/Image.gz, device: qemu-arm64, tests: [ltp-fsx]}
    - {kernel: https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/Image.gz, device: qemu-arm64, tests: [ltp-nptl]}
    - {kernel: https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/Image.gz, device: qemu-arm64, tests: [ltp-smoke]}
```

Submit the multiple test request using the tuxsuite command line interface. This will
wait for all the test to complete before returning by default.

```shell
tuxsuite plan plan.yaml
```

The output will look like:

```shell
Running Linux Kernel plan 'kernel validation': 'Build and test linux kernel variants'
Plan https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uFktpAZAjIwoiRuQvQn5BNBiLO

uid: 1uFktpAZAjIwoiRuQvQn5BNBiLO
⚙️ Provisioning: [boot, ltp-fcntl-locktests] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFktxejy79i0yzOCDUiPMS7gvx
⚙️ Provisioning: [boot, ltp-fs_bind] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFktun3uh3lIloJ1dldAPgtqhb
⚙️ Provisioning: [boot, ltp-fsx] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFkttuCCfxqWe9imPHkbQvi9V3
⚙️ Provisioning: [boot, ltp-nptl] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFktw21TnDbb9Qw0EfodYR9SLc
⚙️ Provisioning: [boot, ltp-smoke] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFkty5C1Z52muy3lg05AvzB0sl
🚀 Running: [boot, ltp-fcntl-locktests] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFktxejy79i0yzOCDUiPMS7gvx
🚀 Running: [boot, ltp-fs_bind] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFktun3uh3lIloJ1dldAPgtqhb
🚀 Running: [boot, ltp-fsx] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFkttuCCfxqWe9imPHkbQvi9V3
🚀 Running: [boot, ltp-nptl] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFktw21TnDbb9Qw0EfodYR9SLc
🚀 Running: [boot, ltp-smoke] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFkty5C1Z52muy3lg05AvzB0sl
🎉 Pass: [boot, ltp-fcntl-locktests] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFktxejy79i0yzOCDUiPMS7gvx
🎉 Pass: [boot, ltp-fsx] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFkttuCCfxqWe9imPHkbQvi9V3
🎉 Pass: [boot, ltp-smoke] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFkty5C1Z52muy3lg05AvzB0sl
🎉 Pass: [boot, ltp-nptl] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFktw21TnDbb9Qw0EfodYR9SLc
🎉 Pass: [boot, ltp-fs_bind] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFktun3uh3lIloJ1dldAPgtqhb
```

The [plan details](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uFktpAZAjIwoiRuQvQn5BNBiLO)
are available at a non-guessable URL, along with links to each individual tests.

!!! tip "tuxsuite scalability"
    Thanks to TuxSuite, in less than 6 minutes, you were able to test the
    given linux kernel with 5 ltp test suites.


## Multiple kernels

Using the given plan configuration file, you will test the many variants of the
same kernel with multiple test suites.

```yaml
version: 1
name: kernel validation
description: Build and test linux kernel variants
jobs:
- name: armv5
  tests:
    - {kernel: https://builds.tuxbuild.com/1uGNWaTO60VHIBbQ9UJSDRhIiTs/zImage, device: qemu-armv5, tests: [ltp-fcntl-locktests]}
    - {kernel: https://builds.tuxbuild.com/1uGNWaTO60VHIBbQ9UJSDRhIiTs/zImage, device: qemu-armv5, tests: [ltp-fs_bind]}
    - {kernel: https://builds.tuxbuild.com/1uGNWaTO60VHIBbQ9UJSDRhIiTs/zImage, device: qemu-armv5, tests: [ltp-fsx]}
    - {kernel: https://builds.tuxbuild.com/1uGNWaTO60VHIBbQ9UJSDRhIiTs/zImage, device: qemu-armv5, tests: [ltp-nptl]}
    - {kernel: https://builds.tuxbuild.com/1uGNWaTO60VHIBbQ9UJSDRhIiTs/zImage, device: qemu-armv5, tests: [ltp-smoke]}
- name: armv7
  tests:
    - {kernel: https://builds.tuxbuild.com/1uGNWWojBf8pqjEUr4gfPrJHxWH/zImage, device: qemu-armv7, tests: [ltp-fcntl-locktests]}
    - {kernel: https://builds.tuxbuild.com/1uGNWWojBf8pqjEUr4gfPrJHxWH/zImage, device: qemu-armv7, tests: [ltp-fs_bind]}
    - {kernel: https://builds.tuxbuild.com/1uGNWWojBf8pqjEUr4gfPrJHxWH/zImage, device: qemu-armv7, tests: [ltp-fsx]}
    - {kernel: https://builds.tuxbuild.com/1uGNWWojBf8pqjEUr4gfPrJHxWH/zImage, device: qemu-armv7, tests: [ltp-nptl]}
    - {kernel: https://builds.tuxbuild.com/1uGNWWojBf8pqjEUr4gfPrJHxWH/zImage, device: qemu-armv7, tests: [ltp-smoke]}
- name: arm64
  tests:
    - {kernel: https://builds.tuxbuild.com/1uGNWanBoMJ7AAdvGipLbaD7nRu/Image.gz, device: qemu-arm64, tests: [ltp-fcntl-locktests]}
    - {kernel: https://builds.tuxbuild.com/1uGNWanBoMJ7AAdvGipLbaD7nRu/Image.gz, device: qemu-arm64, tests: [ltp-fs_bind]}
    - {kernel: https://builds.tuxbuild.com/1uGNWanBoMJ7AAdvGipLbaD7nRu/Image.gz, device: qemu-arm64, tests: [ltp-fsx]}
    - {kernel: https://builds.tuxbuild.com/1uGNWanBoMJ7AAdvGipLbaD7nRu/Image.gz, device: qemu-arm64, tests: [ltp-nptl]}
    - {kernel: https://builds.tuxbuild.com/1uGNWanBoMJ7AAdvGipLbaD7nRu/Image.gz, device: qemu-arm64, tests: [ltp-smoke]}
```

Submit the multiple test request using the tuxsuite command line interface. This will
wait for all the test to complete before returning by default.

```shell
tuxsuite plan plan.yaml
```

The output will look like:

```shell
Running Linux Kernel plan 'kernel validation': 'Build and test linux kernel variants'
Plan https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uGPlbYRuyCjI3kTpIqMn1nxtCA

uid: 1uGPlbYRuyCjI3kTpIqMn1nxtCA
⚙️ Provisioning: [boot, ltp-fcntl-locktests] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlireB51l9s84ms9JBxVvbfd
⚙️ Provisioning: [boot, ltp-fs_bind] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlnzBJ4GBsqVOJmg6xLGxIfV
⚙️ Provisioning: [boot, ltp-fsx] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPliycdAtHYFYMrz5VxbgFNVg
⚙️ Provisioning: [boot, ltp-nptl] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPllXV72sPhtg95Z8IqbXt6dY
⚙️ Provisioning: [boot, ltp-smoke] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPliDH22jCt7BLHtAowkBudVA
⚙️ Provisioning: [boot, ltp-fcntl-locktests] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPllrlkQ6IedfIYlefaOFHvZr
⚙️ Provisioning: [boot, ltp-fs_bind] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlm3gR0zuEyQxH0wuzRbokTx
⚙️ Provisioning: [boot, ltp-fsx] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlm9x6Ek6lejxUpVeZy4Vhab
⚙️ Provisioning: [boot, ltp-nptl] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlm0jgjFsUj0onjWLBv0Exop
⚙️ Provisioning: [boot, ltp-smoke] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPljGoljLZYhgoCPqAPG4nxM4
⚙️ Provisioning: [boot, ltp-fcntl-locktests] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlnjonxUAW1RfqejEUCTwGMX
⚙️ Provisioning: [boot, ltp-fs_bind] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlougnP5iHI7RMwWP74w7tPa
⚙️ Provisioning: [boot, ltp-fsx] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlobXzyBiGa2QKaqcgjy2zFc
⚙️ Provisioning: [boot, ltp-nptl] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlivlAERQza5tFwQ85iqF2eo
⚙️ Provisioning: [boot, ltp-smoke] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlpNsHJSynh9v38vzV3dt6KI
🚀 Running: [boot, ltp-fs_bind] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlougnP5iHI7RMwWP74w7tPa
🚀 Running: [boot, ltp-fcntl-locktests] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlireB51l9s84ms9JBxVvbfd
🚀 Running: [boot, ltp-fs_bind] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlnzBJ4GBsqVOJmg6xLGxIfV
🚀 Running: [boot, ltp-fsx] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPliycdAtHYFYMrz5VxbgFNVg
🚀 Running: [boot, ltp-nptl] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPllXV72sPhtg95Z8IqbXt6dY
🚀 Running: [boot, ltp-smoke] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPliDH22jCt7BLHtAowkBudVA
🚀 Running: [boot, ltp-fcntl-locktests] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPllrlkQ6IedfIYlefaOFHvZr
🚀 Running: [boot, ltp-fcntl-locktests] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlnjonxUAW1RfqejEUCTwGMX
🚀 Running: [boot, ltp-fsx] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlobXzyBiGa2QKaqcgjy2zFc
🚀 Running: [boot, ltp-nptl] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlivlAERQza5tFwQ85iqF2eo
🚀 Running: [boot, ltp-fs_bind] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlm3gR0zuEyQxH0wuzRbokTx
🚀 Running: [boot, ltp-smoke] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlpNsHJSynh9v38vzV3dt6KI
🎉 Pass: [boot, ltp-smoke] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPliDH22jCt7BLHtAowkBudVA
🎉 Pass: [boot, ltp-fsx] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlobXzyBiGa2QKaqcgjy2zFc
🎉 Pass: [boot, ltp-fcntl-locktests] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPllrlkQ6IedfIYlefaOFHvZr
🎉 Pass: [boot, ltp-fcntl-locktests] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlnjonxUAW1RfqejEUCTwGMX
🎉 Pass: [boot, ltp-smoke] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlpNsHJSynh9v38vzV3dt6KI
🎉 Pass: [boot, ltp-fcntl-locktests] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlireB51l9s84ms9JBxVvbfd
🎉 Pass: [boot, ltp-fsx] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPliycdAtHYFYMrz5VxbgFNVg
🎉 Pass: [boot, ltp-nptl] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPllXV72sPhtg95Z8IqbXt6dY
🎉 Pass: [boot, ltp-nptl] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlivlAERQza5tFwQ85iqF2eo
🎉 Pass: [boot, ltp-smoke] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPljGoljLZYhgoCPqAPG4nxM4
🎉 Pass: [boot, ltp-fsx] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlm9x6Ek6lejxUpVeZy4Vhab
🎉 Pass: [boot, ltp-nptl] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlm0jgjFsUj0onjWLBv0Exop
🎉 Pass: [boot, ltp-fs_bind] qemu-armv5 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlnzBJ4GBsqVOJmg6xLGxIfV
🎉 Pass: [boot, ltp-fs_bind] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlm3gR0zuEyQxH0wuzRbokTx
🎉 Pass: [boot, ltp-fs_bind] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uGPlougnP5iHI7RMwWP74w7tPa
```

The [plan details](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uGPlbYRuyCjI3kTpIqMn1nxtCA)
are available at a non-guessable URL, along with links to each individual tests.

!!! tip "tuxsuite scalability"
    Thanks to TuxSuite, in less than 6 minutes, you were able to run 5 ltp on
    3 different kernel variants.
    In fact, testing one kernel or 3 took the exact same time for you.
