# Running an Lava job definition for FVP target in TuxSuite

Submit a LAVA Job request using the tuxsuite command line interface. Device type fvp-lava has been added to allow users to run legacy LAVA job definition in TuxSuite. Any other parameters passed to TuxSuite will be ignored

```shell
$ tuxsuite test --device fvp-lava --job-definition tuxsuite/examples/tests/job-definition.yaml
```

The output will look like:

```shell
Testing None on fvp-lava with boot
uid: 2V0iVfy0fDmWC2rCyECVv7OrUt6
⚙️  Provisioning: [boot] fvp-lava @ https://tuxapi.tuxsuite.com/v1/groups/demo/projects/demo/tests/2V0iVfy0fDmWC2rCyECVv7OrUt6
🚀 Running: [boot] fvp-lava @ https://tuxapi.tuxsuite.com/v1/groups/demo/projects/demo/tests/2V0iVfy0fDmWC2rCyECVv7OrUt6
🎉 Pass: [boot] fvp-lava @ https://tuxapi.tuxsuite.com/v1/groups/demo/projects/demo/tests/2V0iVfy0fDmWC2rCyECVv7OrUt6
```
