# Testing

Submit a test request using the tuxsuite command line interface. This will
wait for the test to complete before returning by default.

```shell
tuxsuite test \
    --device qemu-arm64 \
    --kernel https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/Image.gz \
    --modules https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/modules.tar.xz
```

The output will look like:

```shell
Testing https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/Image.gz on qemu-arm64 with boot
uid: 1uFgOe2H7UStAvEznAvAL77Qts5
⚙️ Provisioning: [boot] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5
🚀 Running: [boot] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5
🎉 Pass: [boot] qemu-arm64 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5
```

The results
([logs](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5/logs?format=html),
[results](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5/results),
...) will be available at
[tuxapi.tuxsuite.com](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uFgOe2H7UStAvEznAvAL77Qts5)
under a unique and non-guessable URL.

## Devices

The following devices are currently supported by [TuxRun](https://tuxrun.org/devices/), hence TuxTest.

### FVP devices

Device              | FVP version     |OS       |
--------------------|-----------------|---------|
fvp-morello-android | Morello 0.11.27 | Android |
fvp-morello-busybox | Morello 0.11.27 | Busybox |
fvp-morello-oe      | Morello 0.11.27 | OE      |
fvp-morello-ubuntu  | Morello 0.11.27 | Ubuntu  |

### QEMU devices

Device        | Description         | Machine     | CPU              | Kernel
--------------|---------------------|-------------|------------------|--------
qemu-arm64    | 64-bit ARMv8        | virt        | Cortex-A57       | Image
qemu-arm64be  | 64-bit ARMv8 (BE)   | virt        | Cortex-A57       | Image
qemu-armv5    | 32-bit ARM          | versatilepb | arm926           | zImage
qemu-armv7    | 32-bit ARM          | virt        | Cortex-A15       | zImage
qemu-armv7be  | 32-bit ARM (BE)     | virt        | Cortex-A15       | zImage
qemu-i386     | 32-bit X86          | q35         | coreduo          | bzImage
qemu-mips32   | 32-bit MIPS         | malta       | mips32r6-generic | vmlinux
qemu-mips32el | 32-bit MIPS (EL)    | malta       | mips32r6-generic | vmlinux
qemu-mips64   | 64-bit MIPS         | malta       | 20Kc             | vmlinux
qemu-mips64el | 64-bit MIPS (EL)    | malta       | 20Kc             | vmlinux
qemu-ppc32    | 32-bit PowerPC      | ppce500     | e500mc           | uImage
qemu-ppc64    | 64-bit PowerPC      | pSeries     | Power8           | vmlinux
qemu-ppc64le  | 64-bit PowerPC (EL) | pSeries     | Power8           | vmlinux
qemu-riscv64  | 64-bit RISC-V       | virt        | rv64             | Image
qemu-s390     | 64-bit s390         | max,zpci=on | s390-ccw-virtio  | bzImage
qemu-sh4      | 32-bit SH           | r2d         | sh7785           | zImage
qemu-sparc64  | 64-bit Sparc        | sun4u       | UltraSPARC II    | vmlinux
qemu-x86_64   | 64-bit X86          | q35         | Nehalem          | bzImage

## Tests

The following devices are currently supported by [TuxRun](https://tuxrun.org/tests/), hence TuxTest.

### FVP devices

Device              | Tests       | Parameters               |
--------------------|-------------|--------------------------|
fvp-morello-android | binder       |                                  |
fvp-morello-android | bionic       | GTEST_FILTER\* BIONIC_TEST_TYPE\*|
fvp-morello-android | boottest     |                                  |
fvp-morello-android | boringssl    | SYSTEM_URL                       |
fvp-morello-android | compartment  | USERDATA                         |
fvp-morello-android | device-tree  |                                  |
fvp-morello-android | dvfs         |                                  |
fvp-morello-android | libjpeg-turbo| LIBJPEG_TURBO_URL, SYSTEM_URL    |
fvp-morello-android | libpdfium    | PDFIUM_URL, SYSTEM_URL           |
fvp-morello-android | libpng       | PNG_URL, SYSTEM_URL              |
fvp-morello-android | lldb         | LLDB_URL, TC_URL                 |
fvp-morello-android | logd         | USERDATA                         |
fvp-morello-android | multicore    |                                  |
fvp-morello-android | zlib         | SYSTEM_URL                       |
fvp-morello-busybox | purecap      |                                  |
fvp-morello-oe      | fwts         |                                  |

!!! tip "Passing parameters"
    In order to pass parameters, use `tuxrun --parameters USERDATA=http://.../userdata.tar.xz`

!!! tip "Default parameters"
    **GTEST_FILTER** is optional and defaults to
    ```
    string_nofortify.*-string_nofortify.strlcat_overread:string_nofortify.bcopy:string_nofortify.memmove
    ```
    **BIONIC_TEST_TYPE** is optional and defaults to `static`. Valid values are `dynamic` and `static`.

### FVP LAVA device

The 'fvp-lava' device type has been specifically added to allow users to execute a FVP [LAVA](https://lava.readthedocs.io/en/latest/) Job definition locally using TuxRun. This device type will not ignore any test cases passed from cli and execute all the tests which are in the LAVA Job definition

More details are available [here](../lava-job-definition)

### Testing on AWS Graviton instances as ssh-device
More details are available [here](../ssh-device)

### QEMU devices

Device  | Tests               | Parameters           |
--------|---------------------|----------------------|
qemu-\* | command             |                      |
qemu-\* | kselftest-gpio      | CPUPOWER\* KSELFTEST |
qemu-\* | kselftest-ipc       | CPUPOWER\* KSELFTEST |
qemu-\* | kselftest-ir        | CPUPOWER\* KSELFTEST |
qemu-\* | kselftest-kcmp      | CPUPOWER\* KSELFTEST |
qemu-\* | kselftest-kexec     | CPUPOWER\* KSELFTEST |
qemu-\* | kselftest-rseq      | CPUPOWER\* KSELFTEST |
qemu-\* | kselftest-rtc       | CPUPOWER\* KSELFTEST |
qemu-\* | kunit\*             |                      |
qemu-\* | ltp-fcntl-locktests |                      |
qemu-\* | ltp-fs_bind         |                      |
qemu-\* | ltp-fs_perms_simple |                      |
qemu-\* | ltp-fsx             |                      |
qemu-\* | ltp-nptl            |                      |
qemu-\* | ltp-smoke           |                      |

In order to specify the test suites to use, use `--tests` like:

!!! tip "Passing parameters"
    In order to pass parameters, use `tuxrun --parameters KSELFTEST=http://.../kselftes.tar.xz`

!!! warning "CPUPOWER"
    Parameter CPUPOWER is only used by *qemu-i386* and *qemu-x86_64*.

!!! warning "KUnit config"
    In order to run KUnit tests, the kernel should be compiled with
    ```
    CONFIG_KUNIT=m
    CONFIG_KUNIT_ALL_TESTS=m
    ```
    The **modules.tar.xz** should be given with `--modules https://.../modules.tar.xz`.

```shell
tuxsuite test \
    --device qemu-arm64 \
    --kernel https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/Image.gz \
    --modules https://builds.tuxbuild.com/1u7Du0Ut6g0qKSd44DMcGeAKsKl/modules.tar.xz
    --tests ltp-fcntl-locktests ltp-fs_bind
```

!!! info "default test suite"
    By default, TuxTest will issue a boot test.
