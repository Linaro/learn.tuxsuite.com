# Reproducing OE builds

!!! warning "WIP"
    This feature is currently a work in progress and might break from time to
    time.

For every OE build triggered using tuxsuite, you can re-run the same build locally.

TuxOE is using [TuxBake](https://gitlab.com/Linaro/tuxbake) for building OE
on both cloud or local host. In order to use the exact same environments, TuxBake
is leveraging [docker](https://docker.com/) with custom container images.

## Setting up

### Host dependencies

You should install `docker` and `python`:

```shell
sudo apt install docker.io
sudo apt install python3 python3-pip
```

### TuxBake

You should now install TuxBake from [PyPi](https://pypi.org/project/tuxbake/):

```shell
python3 -m pip install --upgrade tuxbake
```

## Reproducing

!!! warning "Reproducer script"
    A reproducer script will be provided in the future.

To reproduce
[27368qhx5Qxf2eF1CYAzYntVXLK](https://oebuilds.tuxbuild.com/27368qhx5Qxf2eF1CYAzYntVXLK/),
download
[pinned-manifest.xml](https://oebuilds.tuxbuild.com/27368qhx5Qxf2eF1CYAzYntVXLK/pinned-manifest.xml)
and
[build-definition.json](https://oebuilds.tuxbuild.com/27368qhx5Qxf2eF1CYAzYntVXLK/build-definition.json).

```shell
wget https://oebuilds.tuxbuild.com/27368qhx5Qxf2eF1CYAzYntVXLK/build-definition.json
wget https://oebuilds.tuxbuild.com/27368qhx5Qxf2eF1CYAzYntVXLK/pinned-manifest.xml
tuxbake --build-definition build-definition.json --pinned-manifest pinned-manifest.xml
```
