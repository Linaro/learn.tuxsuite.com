# A single OpenEmbedded build

Submit an OE build request using the tuxsuite command line interface. This will
wait for the OE build to complete before returning by default.

```shell
git clone https://gitlab.com/Linaro/tuxsuite
cd tuxsuite
tuxsuite bake submit examples/bitbake/oniro.json
```

The output will look like:

```shell
*** WARNING: BITBAKE SUPPORT IS EXPERIMENTAL ***
Building target: oniro-image-base with bitbake from {'repo': {'branch': 'kirkstone', 'manifest': 'default.xml', 'url': 'https://booting.oniroproject.org/distro/oniro'}} source with distro: oniro-linux machine: qemux86-64 arguments
uid: 271V1sqR6ulzZFoqr2wstTvlUgV
⚙️  Provisioning: {'repo': {'branch': 'kirkstone', 'manifest': 'default.xml', 'url': 'https://booting.oniroproject.org/distro/oniro'}} with oniro-linux qemux86-64 for oniro-image-base: https://oebuilds.tuxbuild.com/271V1sqR6ulzZFoqr2wstTvlUgV/
🚀 Running: {'repo': {'branch': 'kirkstone', 'manifest': 'default.xml', 'url': 'https://booting.oniroproject.org/distro/oniro'}} with oniro-linux qemux86-64 for oniro-image-base: https://oebuilds.tuxbuild.com/271V1sqR6ulzZFoqr2wstTvlUgV/
🎉 Pass: {'repo': {'branch': 'kirkstone', 'manifest': 'default.xml', 'url': 'https://booting.oniroproject.org/distro/oniro'}} with oniro-linux qemux86-64 for oniro-image-base: https://oebuilds.tuxbuild.com/271V1sqR6ulzZFoqr2wstTvlUgV/
```

The results
([build-definition.json](https://oebuilds.tuxbuild.com/271V1sqR6ulzZFoqr2wstTvlUgV/build-definition.json),
[images](https://oebuilds.tuxbuild.com/271V1sqR6ulzZFoqr2wstTvlUgV/images/),
[licenses](https://oebuilds.tuxbuild.com/271V1sqR6ulzZFoqr2wstTvlUgV/licenses/),
[rpm](https://oebuilds.tuxbuild.com/271V1sqR6ulzZFoqr2wstTvlUgV/rpm/),
[bitbake-environment](https://oebuilds.tuxbuild.com/271V1sqR6ulzZFoqr2wstTvlUgV/bitbake-environment),
[pinned-manifest.xml](https://oebuilds.tuxbuild.com/271V1sqR6ulzZFoqr2wstTvlUgV/pinned-manifest.xml),
[logs](https://oebuilds.tuxbuild.com/271V1sqR6ulzZFoqr2wstTvlUgV/build.log), ...)
will be available at
[artifacts](https://oebuilds.tuxbuild.com/271V1sqR6ulzZFoqr2wstTvlUgV/)
under a unique and non-guessable URL.

## OE Versions

Currently TuxOE supports: `sumo`, `dunfell`, `honister` and `master`

TuxOE allows to build with various ubuntu LTS versions: `16.04`, `18.04` and `20.04`

## Build-definitions

TuxOE uses build-definition to describes the build:

* the layers that should be downloaded
* `local.conf` and `bblayers.conf` to use
* machine type
* distro targets
* ...

TuxOE can downloads the layers using either git protocols or the repo tool.

### Using repo

```json
{
  "container": "ubuntu-20.04",
  "distro": "rpb",
  "envsetup": "setup-environment",
  "machine": "dragonboard-845c",
  "extraconfigs": [],
  "sources": {
    "repo": {
      "branch": "qcom/dunfell",
      "manifest": "default.xml",
      "url": "https://github.com/96boards/oe-rpb-manifest.git"
    }
  },
  "target": "rpb-console-image rpb-console-image-test rpb-desktop-image rpb-desktop-image-test"
}
```

### Using git repositories

```json
{
  "sources": {
    "git_trees": [
      {
        "url": "http://git.yoctoproject.org/git/poky",
        "branch": "honister"
      },
      {
        "url": "https://github.com/ndechesne/meta-qcom",
        "branch": "honister"
      }
    ]
  },
  "container": "ubuntu-20.04",
  "extraconfigs": [],
  "envsetup": "poky/oe-init-build-env",
  "distro": "poky",
  "machine": "dragonboard-845c",
  "target": "core-image-minimal",
  "bblayers_conf": [
    "BBLAYERS += \"../meta-qcom/\""
  ],
  "environment": {}
}

```

### Using Android

```json

{
    "artifacts": [],
    "bblayers_conf": [],
    "container": "ubuntu-20.04",
    "distro": null,
    "environment": {},
    "envsetup": null,
    "local_conf": [],
    "machine": null,
    "name": "",
    "sources": {
        "android": {
            "branch": "common-android11-5.4",
            "build_config": "common/build.config.db845c",
            "manifest": "default.xml",
            "url": "https://android.googlesource.com/kernel/manifest"
        }
    },
    "targets": null
}

```


### Using kas

```json
{
  "sources": {
    "kas": {
      "url": "https://git.codelinaro.org/linaro/dependable-boot/meta-ts",
      "yaml": "ci/zynqmp-starter.yml"
    }
  }
}

```
kas build with a dependent kas override file.

!!! note "kas.json"

```json
{
    "sources": {
        "kas": {
            "branch": "kirkstone-dev",
            "url": "https://gitlab.com/soafee/ewaol/meta-ewaol-machine.git",
            "yaml": "kas/ewaol/baremetal.yml:kas/machine/avadp.yml:kas/ewaol/ci-constraints.yml"
        }
    }
}
```

!!! note "kas_override.yml"
``` yaml
header:
  version: 11

repos:
  meta-ewaol:
    url: https://gitlab.com/soafee/ewaol/meta-ewaol.git
    refspec: refs/merge-requests/11/merge
    path: layers/meta-ewaol
```
To submit a kas build with kas override file:
```shell
tuxsuite bake submit kas.json -k kas_override.yml
```

### OE Build with manifest

```json
{
  "container": "ubuntu-20.04",
  "envsetup": "setup-environment",
  "sources": {
      "repo": {
          "branch": "qcom/dunfell",
          "manifest": "default.xml",
          "url": "https://github.com/96boards/oe-rpb-manifest.git"
      }
  },
  "local_conf": [
      "INHERIT += 'buildstats buildstats-summary'",
      "INHERIT:remove = 'rm_work'"
  ],
  "artifacts": [
      "licenses"
  ],
  "distro": "rpb",
  "target": "intltool-native",
  "machine": "dragonboard-410c",
  "name": "dragonboard-410c-rpb"
}
```

## manifest
```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
<extend-project name="ndechesne/meta-qcom" revision="2d91eb26e3cf0f6434f288518516750ac84329af"/>
</manifest>
```

To submit the build with manifest:

```shell
tuxsuite bake submit build-definition.json --local-manifest local-manifest.xml
```




### Format

The build definition can include the following fields:

* `sources` (dictionary with a single item): could be one **git_trees**, **repo**, **kas** or **android**
* `git_trees` (list of dictionaries): Each dictionary will have **url**, **branch** and optionally **ref**
* `repo` (a dictionary): should have **branch**, **manifest** and **url**, describing where the manifests are hosted along with the branch and manifest file to be used in the build.
* `kas` (a dictionary): Each dictionary will have **url**, **branch**, **yaml** and optionally **ref**. No other fields are allowed when you use kas.
* `android` (a dictionary): Each dictionary will have **url**, **branch**, **build_config**, **manifest** and optionally **bazel**. No other fields are allowed when you use android.
* `distro`: the distro variable passed to OE build
* `extraconfigs` (list of strings): each entry corresponds to some extra configs that will be copied to conf folder and will be using while building the target. It is used only in case of build definition containing sources as **repo** or **git_trees**.
* `envsetup`: path to the script, relative to the source directory, that needs to be sourced to setup bitbake build environment
* `machine`: the machine variable passed to OE build
* `target`: target passed to the bitbake command
* `container`: the container used by Docker to do the build. We currently support **ubuntu-16.04**, **ubuntu-18.04**, **ubuntu-20.04**, **ubuntu-22.04**, **centos-8**, **debian-bullseye**, **debian-buster**, **debian-stretch**, **fedora-33**, **fedora-34**, **opensuse-leap-15.1**, **opensuse-leap-15.2**
* `local_conf` (list of strings): each entry corresponds to a line in local.conf file. The list of string is converted to `local.conf` file
* `bblayers_conf` (list of strings): each entry corresponds to a line in bblayers.conf file. The list of string is converted to `bblayers.conf` file
* `environment` (dictionary of environment variables): they are set before calling bitbake

### Examples

* [Layers with Repo](https://gitlab.com/Linaro/tuxsuite/-/blob/master/examples/bitbake/ledge-rpb.json)
* [Layers from git](https://gitlab.com/Linaro/tuxsuite/-/blob/master/examples/bitbake/lt-qcom.json)
* [YOCTO](https://gitlab.com/Linaro/tuxsuite/-/blob/master/examples/bitbake/yocto.json)
