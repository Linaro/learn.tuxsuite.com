# Reproducing builds

For every build triggered using tuxsuite, you can reproduce the exact same binary locally.

TuxBuild is using [TuxMake](https://tuxmake.org) for building the linux kernel
on both cloud or local host. In order to use the exact same toolchain, TuxMake
is leveraging [podman](https://podman.io/) with custom container images.

## Setting up

### Host dependencies

You should install `podman` and `python`:

```shell
sudo apt install podman
sudo apt install python3 python3-pip
```

### TuxMake

You should now install TuxMake from [PyPi](https://pypi.org/project/tuxmake/):

```shell
python3 -m pip install --upgrade tuxmake
```

## Reproducing

To reproduce
[1uFVhFSO9vz7rwAIvgEBRF1wltH](https://builds.tuxbuild.com/1uFVhFSO9vz7rwAIvgEBRF1wltH/),
download
[tuxmake_reproducer.sh](https://builds.tuxbuild.com/1uFVhFSO9vz7rwAIvgEBRF1wltH/tuxmake_reproducer.sh)
and execute it inside a linux tree at the right hash.

```shell
wget https://builds.tuxbuild.com/1uFVhFSO9vz7rwAIvgEBRF1wltH/tuxmake_reproducer.sh
bash ./tuxmake_reproducer.sh
```

The output will look like:
```shell
# to reproduce this build locally: tuxmake --target-arch=arm --kconfig=versatile_defconfig --kconfig-add=CONFIG_PCI=y --kconfig-add=CONFIG_PCI_VERSATILE=y --kconfig-add=CONFIG_SCSI=y --kconfig-add=CONFIG_BLK_DEV_SD=y --kconfig-add=CONFIG_SCSI_SYM53C8XX_2=y --kconfig-add=CONFIG_EXT4_FS=y --toolchain=gcc-11 --wrapper=none --environment=KBUILD_BUILD_TIMESTAMP=@1624226595 --environment=KBUILD_BUILD_USER=tuxmake --environment=KBUILD_BUILD_HOST=tuxmake --runtime=podman --image=docker.io/tuxmake/arm_gcc-11 config default kernel xipkernel modules dtbs dtbs-legacy debugkernel headers
# CONFIG_PCI=y -> /home/user/.cache/tuxmake/builds/current/0.config
# CONFIG_PCI_VERSATILE=y -> /home/user/.cache/tuxmake/builds/current/1.config
# CONFIG_SCSI=y -> /home/user/.cache/tuxmake/builds/current/2.config
# CONFIG_BLK_DEV_SD=y -> /home/user/.cache/tuxmake/builds/current/3.config
# CONFIG_SCSI_SYM53C8XX_2=y -> /home/user/.cache/tuxmake/builds/current/4.config
# CONFIG_EXT4_FS=y -> /home/user/.cache/tuxmake/builds/current/5.config
make --silent --keep-going --jobs=4 O=/home/user/.cache/tuxmake/builds/current ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- versatile_defconfig
[...]
make --silent --keep-going --jobs=4 O=/home/user/.cache/tuxmake/builds/current ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf-
[...]
I: config: PASS in 0:00:06.388861
I: default: PASS in 0:07:28.314187
I: kernel: PASS in 0:00:11.421375
I: xipkernel: SKIP in 0:00:00.244917
I: modules: PASS in 0:00:01.740093
I: dtbs: PASS in 0:00:07.384224
I: dtbs-legacy: SKIP in 0:00:00.483536
I: debugkernel: PASS in 0:00:06.772933
I: headers: PASS in 0:00:08.728308
I: build output in /home/user/.cache/tuxmake/builds/11
```

The binaries will be available in the build output directory.
