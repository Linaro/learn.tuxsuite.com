# Multiple builds

Using the TuxSuite command line you can build multiple variants of the same git
tree in parallel, making use of the scalability of TuxSuite.

!!! tip "tuxsuite plan"
    You can also chain build and test with tuxsuite plan. For each, build a
    set of tests can be automatically ran. See [tuxsuite plan](../plan/kernel/arch.md).

## Multiple architectures

Using the given plan configuration file, you will build the same git-tree for
multiple architectures.

```yaml
version: 1
name: kernel validation
description: Build and test linux kernel variants
jobs:
- name: armv5
  builds:
    - {toolchain: gcc-11, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
- name: armv7
  builds:
    - {toolchain: gcc-11, target_arch: arm, kconfig: multi_v7_defconfig}
- name: arm64
  builds:
    - {toolchain: gcc-11, target_arch: arm64, kconfig: defconfig}
- name: i386
  builds:
    - {toolchain: gcc-11, target_arch: i386, kconfig: defconfig}
- name: mips64
  builds:
    - {toolchain: gcc-10, target_arch: mips, kernel_image: vmlinux, kconfig: [malta_defconfig, "https://storage.tuxboot.com/mips64/malta_defconfig"]}
- name: mips64el
  builds:
    - {toolchain: gcc-10, target_arch: mips, kernel_image: vmlinux, kconfig: [malta_defconfig, "https://storage.tuxboot.com/mips64el/malta_defconfig"]}
- name: ppc64
  builds:
    - {toolchain: gcc-11, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_defconfig}
- name: ppc64le
  builds:
    - {toolchain: gcc-11, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_le_defconfig}
- name: riscv64
  builds:
    - {toolchain: gcc-11, target_arch: riscv, kernel_image: Image, kconfig: defconfig}
- name: sparc64
  builds:
    - {toolchain: gcc-11, target_arch: sparc, kernel_image: vmlinux, kconfig: [sparc64_defconfig]}
- name: x86_64
  builds:
    - {toolchain: gcc-11, target_arch: x86_64, kconfig: defconfig}
```

Submit the multiple build request using the tuxsuite command line interface. This will
wait for all the build to complete before returning by default.

```shell
tuxsuite plan \
    --git-repo "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git" \
    --git-ref "master" \
    plan.yaml
```

The output will look like:

```shell
Running Linux Kernel plan 'kernel validation': 'Build and test linux kernel variants'
Plan https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1u7HnN5KQ5IrBN29zwoXM5NRq2B

uid: 1u7HnN5KQ5IrBN29zwoXM5NRq2B
⚙️ Provisioning: arm (versatile_defconfig+6) with gcc-11 @ https://builds.tuxbuild.com/1u7HnRoURORdMDYXKcq9IUkBzOT/
⚙️ Provisioning: arm (multi_v7_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnQJkHKG1G5JM5meTvV3FO1i/
⚙️ Provisioning: arm64 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnUMb58h2pDCC5ajGB8mmdiq/
⚙️ Provisioning: i386 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnQc3b0VuCRnKsEYms42woSl/
⚙️ Provisioning: mips (malta_defconfig+1) with gcc-10 @ https://builds.tuxbuild.com/1u7HnPmY5LTOaJRxYgSMAtmvyip/
⚙️ Provisioning: mips (malta_defconfig+1) with gcc-10 @ https://builds.tuxbuild.com/1u7HnOAyy4KWe6CT1lmIR55boHK/
⚙️ Provisioning: powerpc (pseries_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnUYp5vfg8nUsa193mHy8Mnb/
⚙️ Provisioning: powerpc (pseries_le_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnRWMMUOAlZGDGJBlGed2feT/
⚙️ Provisioning: riscv (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnOyUgK9yVcPizii6VHdA7dj/
⚙️ Provisioning: sparc (sparc64_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnRhdMGyAq2c0nKBeAneT6Vu/
⚙️ Provisioning: x86_64 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnQHCPcssfF47XZwjjSZT09c/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") arm64 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnUMb58h2pDCC5ajGB8mmdiq/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") i386 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnQc3b0VuCRnKsEYms42woSl/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") mips (malta_defconfig+1) with gcc-10 @ https://builds.tuxbuild.com/1u7HnPmY5LTOaJRxYgSMAtmvyip/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") mips (malta_defconfig+1) with gcc-10 @ https://builds.tuxbuild.com/1u7HnOAyy4KWe6CT1lmIR55boHK/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") powerpc (pseries_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnUYp5vfg8nUsa193mHy8Mnb/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") arm (versatile_defconfig+6) with gcc-11 @ https://builds.tuxbuild.com/1u7HnRoURORdMDYXKcq9IUkBzOT/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") arm (multi_v7_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnQJkHKG1G5JM5meTvV3FO1i/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") powerpc (pseries_le_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnRWMMUOAlZGDGJBlGed2feT/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") riscv (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnOyUgK9yVcPizii6VHdA7dj/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") sparc (sparc64_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnRhdMGyAq2c0nKBeAneT6Vu/
🚀 Running: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") x86_64 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnQHCPcssfF47XZwjjSZT09c/
👹 Fail (3 errors) with status message 'failure while building tuxmake target(s): default': fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") sparc (sparc64_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnRhdMGyAq2c0nKBeAneT6Vu/
🎉 Pass: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") arm (versatile_defconfig+6) with gcc-11 @ https://builds.tuxbuild.com/1u7HnRoURORdMDYXKcq9IUkBzOT/
🎉 Pass: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") riscv (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnOyUgK9yVcPizii6VHdA7dj/
🎉 Pass: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") x86_64 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnQHCPcssfF47XZwjjSZT09c/
👾 Pass (4 warnings): fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") mips (malta_defconfig+1) with gcc-10 @ https://builds.tuxbuild.com/1u7HnOAyy4KWe6CT1lmIR55boHK/
👾 Pass (4 warnings): fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") mips (malta_defconfig+1) with gcc-10 @ https://builds.tuxbuild.com/1u7HnPmY5LTOaJRxYgSMAtmvyip/
🎉 Pass: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") i386 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnQc3b0VuCRnKsEYms42woSl/
👹 Fail (0 errors) with status message 'failure while building tuxmake target(s): default': fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") powerpc (pseries_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnUYp5vfg8nUsa193mHy8Mnb/
👹 Fail (0 errors) with status message 'failure while building tuxmake target(s): default': fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") powerpc (pseries_le_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnRWMMUOAlZGDGJBlGed2feT/
🎉 Pass: fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") arm (multi_v7_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnQJkHKG1G5JM5meTvV3FO1i/
👾 Pass (1 warning): fd0aa1a4567d ("Merge tag 'for-linus' of git://git.kernel.org/pub/scm/virt/kvm/kvm") arm64 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1u7HnUMb58h2pDCC5ajGB8mmdiq/

Summary: https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1u7HnN5KQ5IrBN29zwoXM5NRq2B
1u7HnRoURORdMDYXKcq9IUkBzOT 🎉 Pass arm@gcc-11
1u7HnQJkHKG1G5JM5meTvV3FO1i 🎉 Pass arm@gcc-11
1u7HnQc3b0VuCRnKsEYms42woSl 🎉 Pass i386@gcc-11
1u7HnOyUgK9yVcPizii6VHdA7dj 🎉 Pass riscv@gcc-11
1u7HnQHCPcssfF47XZwjjSZT09c 🎉 Pass x86_64@gcc-11
1u7HnUMb58h2pDCC5ajGB8mmdiq 👾 Pass (1 warning) arm64@gcc-11
1u7HnPmY5LTOaJRxYgSMAtmvyip 👾 Pass (4 warnings) mips@gcc-10
1u7HnOAyy4KWe6CT1lmIR55boHK 👾 Pass (4 warnings) mips@gcc-10
1u7HnUYp5vfg8nUsa193mHy8Mnb 👹 Fail (0 errors) powerpc@gcc-11
1u7HnRWMMUOAlZGDGJBlGed2feT 👹 Fail (0 errors) powerpc@gcc-11
1u7HnRhdMGyAq2c0nKBeAneT6Vu 👹 Fail (3 errors) sparc@gcc-11
```

The [plan details](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1u7HnN5KQ5IrBN29zwoXM5NRq2B)
are available at a non-guessable URL, along with links to each individual builds.

!!! tip "tuxsuite scalability"
    Thanks to TuxSuite, in less than 15 minutes, you were able to build your git tree for 11 variants.

## Multiple toolchains

With the following plan configuration file, you will build the same git-tree for
armv5, armv7 and arm64 with multiple toolchains.

```yaml
version: 1
name: kernel validation
description: Build and test linux kernel variants
jobs:
- name: armv5
  builds:
    - {toolchain: gcc-9, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
    - {toolchain: gcc-10, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
    - {toolchain: gcc-11, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
- name: armv7
  builds:
    - {toolchain: gcc-9, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: gcc-10, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: gcc-11, target_arch: arm, kconfig: multi_v7_defconfig}
- name: arm64
  builds:
    - {toolchain: gcc-9, target_arch: arm64, kconfig: defconfig}
    - {toolchain: gcc-10, target_arch: arm64, kconfig: defconfig}
    - {toolchain: gcc-11, target_arch: arm64, kconfig: defconfig}
```

Submit the multiple build request using the tuxsuite command line interface. This will
wait for all the build to complete before returning by default.

```shell
tuxsuite plan \
    --git-repo "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git" \
    --git-ref "master" \
    plan.yaml
```

The output will look like:

```shell
Running Linux Kernel plan 'kernel validation': 'Build and test linux kernel variants'
Plan https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uGNWNL0HPBav9bwXGTIbELwrzp

uid: 1uGNWNL0HPBav9bwXGTIbELwrzp
⚙️ Provisioning:  arm (versatile_defconfig+6) with gcc-9 @ https://builds.tuxbuild.com/1uGNWawqCYYeMNVA6jviWVhSe4I/
⚙️ Provisioning:  arm (versatile_defconfig+6) with gcc-10 @ https://builds.tuxbuild.com/1uGNWU7XVWp5VCQ9PhKXwihgd4g/
⚙️ Provisioning:  arm (versatile_defconfig+6) with gcc-11 @ https://builds.tuxbuild.com/1uGNWaTO60VHIBbQ9UJSDRhIiTs/
⚙️ Provisioning:  arm (multi_v7_defconfig) with gcc-9 @ https://builds.tuxbuild.com/1uGNWTnBaPtZAQF80U24mLMNFRM/
⚙️ Provisioning:  arm (multi_v7_defconfig) with gcc-10 @ https://builds.tuxbuild.com/1uGNWU43yD9ZgsrIO1Xhq4lFwt1/
⚙️ Provisioning:  arm (multi_v7_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1uGNWWojBf8pqjEUr4gfPrJHxWH/
⚙️ Provisioning:  arm64 (defconfig) with gcc-9 @ https://builds.tuxbuild.com/1uGNWXDyUKEt3m7KqbtAdFjIpID/
⚙️ Provisioning:  arm64 (defconfig) with gcc-10 @ https://builds.tuxbuild.com/1uGNWWiqyxNBxu3cZmxC5ku7FGu/
⚙️ Provisioning:  arm64 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1uGNWanBoMJ7AAdvGipLbaD7nRu/
🚀 Running: 13311e74253f ("Linux 5.13-rc7") arm (versatile_defconfig+6) with gcc-10 @ https://builds.tuxbuild.com/1uGNWU7XVWp5VCQ9PhKXwihgd4g/
🚀 Running: 13311e74253f ("Linux 5.13-rc7") arm (versatile_defconfig+6) with gcc-11 @ https://builds.tuxbuild.com/1uGNWaTO60VHIBbQ9UJSDRhIiTs/
🚀 Running: 13311e74253f ("Linux 5.13-rc7") arm (multi_v7_defconfig) with gcc-9 @ https://builds.tuxbuild.com/1uGNWTnBaPtZAQF80U24mLMNFRM/
🚀 Running: 13311e74253f ("Linux 5.13-rc7") arm (multi_v7_defconfig) with gcc-10 @ https://builds.tuxbuild.com/1uGNWU43yD9ZgsrIO1Xhq4lFwt1/
🚀 Running: 13311e74253f ("Linux 5.13-rc7") arm64 (defconfig) with gcc-9 @ https://builds.tuxbuild.com/1uGNWXDyUKEt3m7KqbtAdFjIpID/
🚀 Running: 13311e74253f ("Linux 5.13-rc7") arm (versatile_defconfig+6) with gcc-9 @ https://builds.tuxbuild.com/1uGNWawqCYYeMNVA6jviWVhSe4I/
🚀 Running: 13311e74253f ("Linux 5.13-rc7") arm (multi_v7_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1uGNWWojBf8pqjEUr4gfPrJHxWH/
🚀 Running: 13311e74253f ("Linux 5.13-rc7") arm64 (defconfig) with gcc-10 @ https://builds.tuxbuild.com/1uGNWWiqyxNBxu3cZmxC5ku7FGu/
🚀 Running: 13311e74253f ("Linux 5.13-rc7") arm64 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1uGNWanBoMJ7AAdvGipLbaD7nRu/
🎉 Pass: 13311e74253f ("Linux 5.13-rc7") arm (versatile_defconfig+6) with gcc-9 @ https://builds.tuxbuild.com/1uGNWawqCYYeMNVA6jviWVhSe4I/
🎉 Pass: 13311e74253f ("Linux 5.13-rc7") arm (versatile_defconfig+6) with gcc-10 @ https://builds.tuxbuild.com/1uGNWU7XVWp5VCQ9PhKXwihgd4g/
🎉 Pass: 13311e74253f ("Linux 5.13-rc7") arm (versatile_defconfig+6) with gcc-11 @ https://builds.tuxbuild.com/1uGNWaTO60VHIBbQ9UJSDRhIiTs/
🎉 Pass: 13311e74253f ("Linux 5.13-rc7") arm (multi_v7_defconfig) with gcc-9 @ https://builds.tuxbuild.com/1uGNWTnBaPtZAQF80U24mLMNFRM/
🎉 Pass: 13311e74253f ("Linux 5.13-rc7") arm (multi_v7_defconfig) with gcc-10 @ https://builds.tuxbuild.com/1uGNWU43yD9ZgsrIO1Xhq4lFwt1/
👾 Pass (1 warning): 13311e74253f ("Linux 5.13-rc7") arm64 (defconfig) with gcc-9 @ https://builds.tuxbuild.com/1uGNWXDyUKEt3m7KqbtAdFjIpID/
👾 Pass (1 warning): 13311e74253f ("Linux 5.13-rc7") arm64 (defconfig) with gcc-10 @ https://builds.tuxbuild.com/1uGNWWiqyxNBxu3cZmxC5ku7FGu/
👾 Pass (1 warning): 13311e74253f ("Linux 5.13-rc7") arm64 (defconfig) with gcc-11 @ https://builds.tuxbuild.com/1uGNWanBoMJ7AAdvGipLbaD7nRu/
🎉 Pass: 13311e74253f ("Linux 5.13-rc7") arm (multi_v7_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1uGNWWojBf8pqjEUr4gfPrJHxWH/

Summary: https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uGNWNL0HPBav9bwXGTIbELwrzp
1uGNWU7XVWp5VCQ9PhKXwihgd4g 🎉 Pass arm@gcc-10
1uGNWU43yD9ZgsrIO1Xhq4lFwt1 🎉 Pass arm@gcc-10
1uGNWaTO60VHIBbQ9UJSDRhIiTs 🎉 Pass arm@gcc-11
1uGNWWojBf8pqjEUr4gfPrJHxWH 🎉 Pass arm@gcc-11
1uGNWawqCYYeMNVA6jviWVhSe4I 🎉 Pass arm@gcc-9
1uGNWTnBaPtZAQF80U24mLMNFRM 🎉 Pass arm@gcc-9
1uGNWWiqyxNBxu3cZmxC5ku7FGu 👾 Pass (1 warning) arm64@gcc-10
1uGNWanBoMJ7AAdvGipLbaD7nRu 👾 Pass (1 warning) arm64@gcc-11
1uGNWXDyUKEt3m7KqbtAdFjIpID 👾 Pass (1 warning) arm64@gcc-9
```

The [plan details](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uGNWNL0HPBav9bwXGTIbELwrzp)
are available at a non-guessable URL, along with links to each individual builds.
