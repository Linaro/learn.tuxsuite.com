# Build with patch series applied

Submit a build request using the tuxsuite command line interface, to
build an LTS kernel tree using the patch series from the git tree
[https://kernel.googlesource.com/pub/scm/linux/kernel/git/stable/stable-queue](https://kernel.googlesource.com/pub/scm/linux/kernel/git/stable/stable-queue)
which will get applied before starting the build in TuxBuild.

```shell
tuxsuite build \
    --git-repo https://kernel.googlesource.com/pub/scm/linux/kernel/git/stable/linux-stable/ \
    --git-ref v5.10.42 \
    --target-arch arm64 \
    --toolchain gcc-9 \
    --kconfig tinyconfig \
    --patch-series ../stable-queue/releases/5.10.43/
```

The output will look like:

```shell
Building Linux Kernel https://kernel.googlesource.com/pub/scm/linux/kernel/git/stable/linux-stable/ at v5.10.42
uid: 1ucyMzY7apTEHKhErUe46gKHycg
⚙️ Provisioning:  arm64 (tinyconfig) with gcc-9 @ https://builds.tuxbuild.com/1ucyMzY7apTEHKhErUe46gKHycg/
🚀 Running: 65859eca4dff ("Linux 5.10.42") arm64 (tinyconfig) with gcc-9 @ https://builds.tuxbuild.com/1ucyMzY7apTEHKhErUe46gKHycg/
👾 Pass: 65859eca4dff ("Linux 5.10.42") arm64 (tinyconfig) with gcc-9 @ https://builds.tuxbuild.com/1ucyMzY7apTEHKhErUe46gKHycg/
```

The results
([kernel](https://builds.tuxbuild.com/1ucyMzY7apTEHKhErUe46gKHycg/vmlinux.xz),
[headers](https://builds.tuxbuild.com/1ucyMzY7apTEHKhErUe46gKHycg/headers.tar.xz),
[logs](https://builds.tuxbuild.com/1ucyMzY7apTEHKhErUe46gKHycg/build.log), ...)
will be available at
[builds.tuxbuild.com](https://builds.tuxbuild.com/1ucyMzY7apTEHKhErUe46gKHycg/)
under a unique and non-guessable URL.

## Patch series format

Currently TuxSuite cli accepts the following patch series formats:

* mbox
* directory
* .tar.gz (gzipped tar archive)

## Patch series from lore.kernel.org

`b4` will be used to pull a patch series directly from a
[lore.kernel.org](https://lore.kernel.org/) mailing list as shown
below:

```shell
tuxsuite build \
    --git-repo https://github.com/torvalds/linux.git \
    --git-ref master \
    --target-arch arm64 \
    --toolchain gcc-10 \
    --kconfig tinyconfig \
    --patch-series https://lore.kernel.org/lkml/YmkO7LDc0q38VFlE@kroah.com/raw
```

!!! note "Message-id"
    The `Message-id` of the patch-series can be used instead of the url

    ```sh
    tuxsuite build \
        --git-repo https://github.com/torvalds/linux.git \
        --git-ref master \
        --target-arch arm64 \
        --toolchain gcc-9 \
        --kconfig tinyconfig \
        --patch-series 20220720131221.azqfidkry3cwiarw@bogus
    ```

## Reproducing

The builds done with patch series applied are reproducible locally only after
applying the patch manually.

If needed, the patch series is available at the tuxbuild URL.
