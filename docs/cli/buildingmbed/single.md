# Building Mbed-TLS for one configuration

Submit an Mbed-TLS build request using the tuxsuite command line interface. This will
wait for the build to complete before returning by default.

```shell
git clone https://gitlab.com/Linaro/tuxsuite
cd tuxsuite
tuxsuite bake submit examples/bitbake/mbed.json
```

The output will look like:

```shell
Building targets: None with bitbake from {'mbedtls': {'url': 'https://github.com/Mbed-TLS/mbedtls', 'cmds': "ulimit -f 20971520 && export MBEDTLS_TEST_OUTCOME_FILE='outcome.csv' && ./tests/scripts/all.sh --seed 8 build_armcc"}} source with distro: None machine: None arguments
uid: 2KPNkSKe945G5KFhwQ3Cnp9HdwD
⚙️  Provisioning: {'mbedtls': {'url': 'https://github.com/Mbed-TLS/mbedtls', 'cmds': "ulimit -f 20971520 && export MBEDTLS_TEST_OUTCOME_FILE='outcome.csv' && ./tests/scripts/all.sh --seed 8 build_armcc"}} with None None for None: https://tuxapi.tuxsuite.com/v1/groups/demo/projects/demo/oebuilds/2KPNkSKe945G5KFhwQ3Cnp9HdwD 
🚀 Running: {'mbedtls': {'url': 'https://github.com/Mbed-TLS/mbedtls', 'cmds': "ulimit -f 20971520 && export MBEDTLS_TEST_OUTCOME_FILE='outcome.csv' && ./tests/scripts/all.sh --seed 8 build_armcc"}} with None None for None: https://tuxapi.tuxsuite.com/v1/groups/demo/projects/demo/oebuilds/2KPNkSKe945G5KFhwQ3Cnp9HdwD 
🎉 Pass: {'mbedtls': {'url': 'https://github.com/Mbed-TLS/mbedtls', 'cmds': "ulimit -f 20971520 && export MBEDTLS_TEST_OUTCOME_FILE='outcome.csv' && ./tests/scripts/all.sh --seed 8 build_armcc"}} with None None for None: https://tuxapi.tuxsuite.com/v1/groups/demo/projects/demo/oebuilds/2KPNkSKe945G5KFhwQ3Cnp9HdwD 
```

The results
([build-definition.json](https://storage.tuxsuite.com/public/demo/demo/oebuilds/2KPNkSKe945G5KFhwQ3Cnp9HdwD/build-definition.json),
[logs](https://storage.tuxsuite.com/public/demo/demo/oebuilds/2KPNkSKe945G5KFhwQ3Cnp9HdwD/build.log), ...)
will be available at
[artifacts](https://storage.tuxsuite.com/public/demo/demo/oebuilds/2KPNkSKe945G5KFhwQ3Cnp9HdwD/)
under a unique and non-guessable URL.

## Build-definitions

Mbed-TLS uses build-definition to describes the build:

### Using repo

```json
{
  "container": "tuxbake/mbedtls-ubuntu-16.04",
  "sources": {
    "mbedtls": {
      "url": "https://github.com/Mbed-TLS/mbedtls",
      "branch": "master",
      "cmds": "ulimit -f 20971520 && export MBEDTLS_TEST_OUTCOME_FILE='outcome.csv' && ./tests/scripts/all.sh --seed 8 build_armcc"
    }
  }
}
```


### Format

The build definition can include the following fields:

* `sources` (dictionary with a single item): should be mbedtls. You can specify url, branch or ref as well here. The cmds is the command used to do the specific build
* `container`: the container used by Docker to do the build. We currently provide containers tuxbake/mbedtls-ubuntu-16.04, tuxbake/mbedtls-ubuntu-18.04, tuxbake/mbedtls-ubuntu-20.04

### Plan
This single plan that does all the builds that corresponding to CI for mbed-TLS for Linux:

[mbed.yaml](https://gitlab.com/Linaro/tuxsuite/-/blob/master/examples/bitbake/mbed.yaml)

Here is the plan that has done the full build with the above plan:
[mbed-results](https://tuxapi.tuxsuite.com/v1/groups/demo/projects/demo/plans/2KReAZr8wxioSqPzr2Fnst8gaop)
