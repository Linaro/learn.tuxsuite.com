# Full kernel validation

A plan is a combination of builds and tests.

For each build, it's possible to define tests that would be triggered when the
build is successful. This dependency is declared at submission time and later
handled directly by the server.

## Plan configuration

This plan will build the given linux kernel for every supported architecture
with every available toolchains. Each build will then be tested with every
available LTP test suites.

```yaml
version: 1
name: kernel validation
description: Build and test linux kernel with every toolchain
jobs:
- name: armv5
  builds:
    - {toolchain: gcc-8, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
    - {toolchain: gcc-9, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
    - {toolchain: gcc-10, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
    - {toolchain: gcc-11, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
    - {toolchain: clang-10, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
    - {toolchain: clang-11, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
    - {toolchain: clang-12, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
    - {toolchain: clang-nightly, target_arch: arm, kconfig: [versatile_defconfig, "CONFIG_PCI=y", "CONFIG_PCI_VERSATILE=y", "CONFIG_SCSI=y", "CONFIG_BLK_DEV_SD=y", "CONFIG_SCSI_SYM53C8XX_2=y", "CONFIG_EXT4_FS=y"]}
  tests:
    - {device: qemu-armv5, tests: [ltp-fcntl-locktests]}
    - {device: qemu-armv5, tests: [ltp-fs_bind]}
    - {device: qemu-armv5, tests: [ltp-fsx]}
    - {device: qemu-armv5, tests: [ltp-nptl]}
    - {device: qemu-armv5, tests: [ltp-smoke]}
- name: armv7
  builds:
    - {toolchain: gcc-8, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: gcc-9, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: gcc-10, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: gcc-11, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: clang-10, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: clang-11, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: clang-12, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: clang-nightly, target_arch: arm, kconfig: multi_v7_defconfig}
  tests:
    - {device: qemu-armv7, tests: [ltp-fcntl-locktests]}
    - {device: qemu-armv7, tests: [ltp-fs_bind]}
    - {device: qemu-armv7, tests: [ltp-fsx]}
    - {device: qemu-armv7, tests: [ltp-nptl]}
    - {device: qemu-armv7, tests: [ltp-smoke]}
- name: arm64
  builds:
    - {toolchain: gcc-8, target_arch: arm64, kconfig: defconfig}
    - {toolchain: gcc-9, target_arch: arm64, kconfig: defconfig}
    - {toolchain: gcc-10, target_arch: arm64, kconfig: defconfig}
    - {toolchain: gcc-11, target_arch: arm64, kconfig: defconfig}
    - {toolchain: clang-10, target_arch: arm64, kconfig: defconfig}
    - {toolchain: clang-11, target_arch: arm64, kconfig: defconfig}
    - {toolchain: clang-12, target_arch: arm64, kconfig: defconfig}
    - {toolchain: clang-nightly, target_arch: arm64, kconfig: defconfig}
  tests:
    - {device: qemu-arm64, tests: [ltp-fcntl-locktests]}
    - {device: qemu-arm64, tests: [ltp-fs_bind]}
    - {device: qemu-arm64, tests: [ltp-fsx]}
    - {device: qemu-arm64, tests: [ltp-nptl]}
    - {device: qemu-arm64, tests: [ltp-smoke]}
- name: i386
  builds:
    - {toolchain: gcc-8, target_arch: i386, kconfig: defconfig}
    - {toolchain: gcc-9, target_arch: i386, kconfig: defconfig}
    - {toolchain: gcc-10, target_arch: i386, kconfig: defconfig}
    - {toolchain: gcc-11, target_arch: i386, kconfig: defconfig}
    - {toolchain: clang-10, target_arch: i386, kconfig: defconfig}
    - {toolchain: clang-11, target_arch: i386, kconfig: defconfig}
    - {toolchain: clang-12, target_arch: i386, kconfig: defconfig}
    - {toolchain: clang-nightly, target_arch: i386, kconfig: defconfig}
  tests:
    - {device: qemu-i386, tests: [ltp-fcntl-locktests]}
    - {device: qemu-i386, tests: [ltp-fs_bind]}
    - {device: qemu-i386, tests: [ltp-fsx]}
    - {device: qemu-i386, tests: [ltp-nptl]}
    - {device: qemu-i386, tests: [ltp-smoke]}
- name: mips64
  builds:
    - {toolchain: gcc-8, target_arch: mips, kernel_image: vmlinux, kconfig: [malta_defconfig, "https://storage.tuxboot.com/mips64/malta_defconfig"]}
    - {toolchain: gcc-9, target_arch: mips, kernel_image: vmlinux, kconfig: [malta_defconfig, "https://storage.tuxboot.com/mips64/malta_defconfig"]}
    - {toolchain: gcc-10, target_arch: mips, kernel_image: vmlinux, kconfig: [malta_defconfig, "https://storage.tuxboot.com/mips64/malta_defconfig"]}
  tests:
    - {device: qemu-mips64, tests: [ltp-fcntl-locktests]}
    - {device: qemu-mips64, tests: [ltp-fs_bind]}
    - {device: qemu-mips64, tests: [ltp-fsx]}
    - {device: qemu-mips64, tests: [ltp-nptl]}
    - {device: qemu-mips64, tests: [ltp-smoke]}
- name: mips64el
  builds:
    - {toolchain: gcc-8, target_arch: mips, kernel_image: vmlinux, kconfig: [malta_defconfig, "https://storage.tuxboot.com/mips64el/malta_defconfig"]}
    - {toolchain: gcc-9, target_arch: mips, kernel_image: vmlinux, kconfig: [malta_defconfig, "https://storage.tuxboot.com/mips64el/malta_defconfig"]}
    - {toolchain: gcc-10, target_arch: mips, kernel_image: vmlinux, kconfig: [malta_defconfig, "https://storage.tuxboot.com/mips64el/malta_defconfig"]}
  tests:
    - {device: qemu-mips64el, tests: [ltp-fcntl-locktests]}
    - {device: qemu-mips64el, tests: [ltp-fs_bind]}
    - {device: qemu-mips64el, tests: [ltp-fsx]}
    - {device: qemu-mips64el, tests: [ltp-nptl]}
    - {device: qemu-mips64el, tests: [ltp-smoke]}
- name: ppc64
  builds:
    - {toolchain: gcc-8, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_defconfig}
    - {toolchain: gcc-9, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_defconfig}
    - {toolchain: gcc-10, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_defconfig}
    - {toolchain: gcc-11, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_defconfig}
    - {toolchain: clang-nightly, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_defconfig}
  tests:
    - {device: qemu-ppc64, tests: [ltp-fcntl-locktests]}
    - {device: qemu-ppc64, tests: [ltp-fs_bind]}
    - {device: qemu-ppc64, tests: [ltp-fsx]}
    - {device: qemu-ppc64, tests: [ltp-nptl]}
    - {device: qemu-ppc64, tests: [ltp-smoke]}
- name: ppc64le
  builds:
    - {toolchain: gcc-8, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_le_defconfig}
    - {toolchain: gcc-9, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_le_defconfig}
    - {toolchain: gcc-10, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_le_defconfig}
    - {toolchain: gcc-11, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_le_defconfig}
    - {toolchain: clang-nightly, target_arch: powerpc, kernel_image: vmlinux, kconfig: pseries_le_defconfig}
  tests:
    - {device: qemu-ppc64le, tests: [ltp-fcntl-locktests]}
    - {device: qemu-ppc64le, tests: [ltp-fs_bind]}
    - {device: qemu-ppc64le, tests: [ltp-fsx]}
    - {device: qemu-ppc64le, tests: [ltp-nptl]}
    - {device: qemu-ppc64le, tests: [ltp-smoke]}
- name: riscv64
  builds:
    - {toolchain: gcc-8, target_arch: riscv, kernel_image: Image, kconfig: defconfig}
    - {toolchain: gcc-9, target_arch: riscv, kernel_image: Image, kconfig: defconfig}
    - {toolchain: gcc-10, target_arch: riscv, kernel_image: Image, kconfig: defconfig}
    - {toolchain: gcc-11, target_arch: riscv, kernel_image: Image, kconfig: defconfig}
    - {toolchain: clang-10, target_arch: riscv, kernel_image: Image, kconfig: defconfig}
    - {toolchain: clang-11, target_arch: riscv, kernel_image: Image, kconfig: defconfig}
    - {toolchain: clang-12, target_arch: riscv, kernel_image: Image, kconfig: defconfig}
    - {toolchain: clang-nightly, target_arch: riscv, kernel_image: Image, kconfig: defconfig}
  tests:
    - {device: qemu-riscv64, tests: [ltp-fcntl-locktests]}
    - {device: qemu-riscv64, tests: [ltp-fs_bind]}
    - {device: qemu-riscv64, tests: [ltp-fsx]}
    - {device: qemu-riscv64, tests: [ltp-nptl]}
    - {device: qemu-riscv64, tests: [ltp-smoke]}
- name: sparc64
  builds:
    - {toolchain: gcc-8, target_arch: sparc, kernel_image: vmlinux, kconfig: [sparc64_defconfig]}
    - {toolchain: gcc-9, target_arch: sparc, kernel_image: vmlinux, kconfig: [sparc64_defconfig]}
    - {toolchain: gcc-10, target_arch: sparc, kernel_image: vmlinux, kconfig: [sparc64_defconfig]}
    - {toolchain: gcc-11, target_arch: sparc, kernel_image: vmlinux, kconfig: [sparc64_defconfig]}
  tests:
    - {device: qemu-sparc64, tests: [ltp-fcntl-locktests]}
    - {device: qemu-sparc64, tests: [ltp-fs_bind]}
    - {device: qemu-sparc64, tests: [ltp-fsx]}
    - {device: qemu-sparc64, tests: [ltp-nptl]}
    - {device: qemu-sparc64, tests: [ltp-smoke]}
- name: x86_64
  builds:
    - {toolchain: gcc-8, target_arch: x86_64, kconfig: defconfig}
    - {toolchain: gcc-9, target_arch: x86_64, kconfig: defconfig}
    - {toolchain: gcc-10, target_arch: x86_64, kconfig: defconfig}
    - {toolchain: gcc-11, target_arch: x86_64, kconfig: defconfig}
    - {toolchain: clang-10, target_arch: x86_64, kconfig: defconfig}
    - {toolchain: clang-11, target_arch: x86_64, kconfig: defconfig}
    - {toolchain: clang-12, target_arch: x86_64, kconfig: defconfig}
    - {toolchain: clang-nightly, target_arch: x86_64, kconfig: defconfig}
  tests:
    - {device: qemu-x86_64, tests: [ltp-fcntl-locktests]}
    - {device: qemu-x86_64, tests: [ltp-fs_bind]}
    - {device: qemu-x86_64, tests: [ltp-fsx]}
    - {device: qemu-x86_64, tests: [ltp-nptl]}
    - {device: qemu-x86_64, tests: [ltp-smoke]}
```

## Command line

Submit the multiple build request using the tuxsuite command line interface. This will
wait for all the build and test to complete before returning by default.

```shell
tuxsuite plan \
    --git-repo "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git" \
    --git-ref "master" \
    plan.yaml
```

## Outputs
The output will look like:

```shell
Running Linux Kernel plan 'kernel validation': 'Build and test linux kernel with every toolchain'
Plan https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uIavoQmdr5lRisqJWSZfL2iRCx

uid: 1uIavoQmdr5lRisqJWSZfL2iRCx
⚙️  Provisioning:  arm (versatile_defconfig+6) with gcc-8 @ https://builds.tuxbuild.com/1uIavvqLuIhXAhUoCrHbInNP1lx/
⚙️  Provisioning:  arm (versatile_defconfig+6) with gcc-9 @ https://builds.tuxbuild.com/1uIavt1pnFlbG6KqneGzuC7Y4wx/
⚙️  Provisioning:  arm (versatile_defconfig+6) with gcc-10 @ https://builds.tuxbuild.com/1uIavqKyCX8wmrP2rLV3aUNx1Tc/
⚙️  Provisioning:  arm (versatile_defconfig+6) with gcc-11 @ https://builds.tuxbuild.com/1uIavu4fzLvnpv6xnm2Kt6pjgFs/
⚙️  Provisioning:  arm (versatile_defconfig+6) with clang-10 @ https://builds.tuxbuild.com/1uIavxCYLdNuQrLxAYolW7IOTS1/
⚙️  Provisioning:  arm (versatile_defconfig+6) with clang-11 @ https://builds.tuxbuild.com/1uIavvNvZnVajG0s2be0lHX0BlH/
⚙️  Provisioning:  arm (versatile_defconfig+6) with clang-12 @ https://builds.tuxbuild.com/1uIavurR7vu66JMCUJVaZchj1ZP/
⚙️  Provisioning:  arm (versatile_defconfig+6) with clang-nightly @ https://builds.tuxbuild.com/1uIaw5L5aigg8DX4KKQVniZTEzq/
⚙️  Provisioning:  arm (multi_v7_defconfig) with gcc-8 @ https://builds.tuxbuild.com/1uIavyhIp8gsgCxRDegvT6pnNGT/
⚙️  Provisioning:  arm (multi_v7_defconfig) with gcc-9 @ https://builds.tuxbuild.com/1uIavzV05EbbaiXxbEHDU3V63i6/
⚙️  Provisioning:  arm (multi_v7_defconfig) with gcc-10 @ https://builds.tuxbuild.com/1uIaw08qrqrA6xi8vXOi4wphOER/
[...]
Summary: https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uIavoQmdr5lRisqJWSZfL2iRCx
1uIavqKyCX8wmrP2rLV3aUNx1Tc 🎉 Pass arm@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw08qrqrA6xi8vXOi4wphOER 🎉 Pass arm@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavu4fzLvnpv6xnm2Kt6pjgFs 🎉 Pass arm@gcc-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw31XPDsLU9aCDy7GM59x5C9 🎉 Pass arm@gcc-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavvqLuIhXAhUoCrHbInNP1lx 🎉 Pass arm@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavyhIp8gsgCxRDegvT6pnNGT 🎉 Pass arm@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavt1pnFlbG6KqneGzuC7Y4wx 🎉 Pass arm@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavzV05EbbaiXxbEHDU3V63i6 🎉 Pass arm@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw3fJnPOnWpdffLV0SKl78KS 🎉 Pass i386@clang-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavyU6uRfg5dAB32HVp6ZW4eJ 🎉 Pass i386@clang-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw0VjH3K3sAKHZJHAJSeD4E0 🎉 Pass i386@clang-12 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw3oqqc5KsWXTpN4pQyrCPel 🎉 Pass i386@clang-nightly 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavyXXfND4mWdYZR4KmXCwz5k 🎉 Pass i386@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw4Z0DDTaBXkhJFPpvamJLXY 🎉 Pass i386@gcc-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw1jUgsl0sroxcJBXwhAVSI4 🎉 Pass i386@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavz9oEFj3Uk4AvriRLK8Xdvw 🎉 Pass i386@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw79jjvI3rv4jls4PzIFBkqj 🎉 Pass powerpc@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw5pPpX4DZV0TwOpgGBYV6tX 🎉 Pass powerpc@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw8rtWj7VqDFt731RgCfCDbe 🎉 Pass powerpc@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawAyNTYB1svDVufHudACh2hX 🎉 Pass powerpc@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw8yMi909VdQrPPErQicifhV 🎉 Pass powerpc@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw6wgyIW8E6MAPLGDZG2jc4e 🎉 Pass powerpc@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw7bvjS9D38j0HrERCugu7Xa 🎉 Pass riscv@clang-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw7KReirjZXDNXJ4QgO6Q5yq 🎉 Pass riscv@clang-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw6GBHcso1ldEbLLgIefwM21 🎉 Pass riscv@clang-12 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawBCNWWxpLrKOwZEhzeTAsqU 🎉 Pass riscv@clang-nightly 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw7HJCb9q8gmtE0FNzUCtviU 🎉 Pass riscv@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawCyMrlqE7cd1yLsubYFnoXk 🎉 Pass riscv@gcc-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawC1IY2FTVQUp8TbXQ3qMyN8 🎉 Pass riscv@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawBKX1tlKvWmQSYzcv5UCeXK 🎉 Pass riscv@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw5QYr7qvOz1wFcghY2wwGq2 🎉 Pass x86_64@clang-12 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw5nQMiKYGrgI88yhVO7DH5i 🎉 Pass x86_64@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawCmCiLXXHXbZYgLNy3ePank 🎉 Pass x86_64@gcc-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawBWnDXjcYNYbK7LAXuLvDpf 🎉 Pass x86_64@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw5hIccDpYDRJa4yCLpAWO7R 🎉 Pass x86_64@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavxCYLdNuQrLxAYolW7IOTS1 👾 Pass (1 warning) arm@clang-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw4gjqA9a27PH0vsRwRMIfKB 👾 Pass (1 warning) arm@clang-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavvNvZnVajG0s2be0lHX0BlH 👾 Pass (1 warning) arm@clang-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavyuETgu7fd1D8tclIo7Ec3v 👾 Pass (1 warning) arm@clang-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavurR7vu66JMCUJVaZchj1ZP 👾 Pass (1 warning) arm@clang-12 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw0CJX7b3QmHhrTt9neQnPa4 👾 Pass (1 warning) arm@clang-12 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw5L5aigg8DX4KKQVniZTEzq 👾 Pass (1 warning) arm@clang-nightly 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavxw1xqXXkbrEEUGjtVOzEuX 👾 Pass (1 warning) arm@clang-nightly 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavxny0lnQ7yq6jWflrmVv5k6 👾 Pass (2140 warnings) arm64@clang-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavyybLFoHJovwLThQEDsH5IB 👾 Pass (1 warning) arm64@clang-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavzedmmLCM7zM2kTkj6US954 👾 Pass (1 warning) arm64@clang-12 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw54ZXbU3v7zaHviKhWutuBm 👾 Pass (1 warning) arm64@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw0YqYUcjNWvvKCOpJyr3K88 👾 Pass (1 warning) arm64@gcc-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw2psLjEc50ntYOwovEEwF9g 👾 Pass (1 warning) arm64@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavzWSRtYK8kAPAHTRAxtIRPi 👾 Pass (1 warning) arm64@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw5JUEz0I9QPmMsQXs18Sko6 👾 Pass (4 warnings) mips@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw2ZvIdVeR8jl5RBOk7GzekB 👾 Pass (4 warnings) mips@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw29lFmEWKC4njF3ZKCnxnSW 👾 Pass (3 warnings) mips@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIavzuZxyAYDsvpnoorOlXGHnr 👾 Pass (3 warnings) mips@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw1rOMpU2VuH2mLhMBmQse8f 👾 Pass (3 warnings) mips@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw3S6QylLR75Tx42ZyDtCRnN 👾 Pass (3 warnings) mips@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawBamcRjKYEKJYvCzQHCSCHi 👾 Pass (3 warnings) sparc@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawAHlOindnPJrMbbjrrKXtsb 👾 Pass (3 warnings) sparc@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw6XofwrMRnKmuOjnDbKucsu 👾 Pass (3 warnings) sparc@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawAdLBq1cPY8FtuFHXZmFCBc 👾 Pass (2 warnings) x86_64@clang-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawAzgHPJ06V4IMouSBPOjkUx 👾 Pass (2 warnings) x86_64@clang-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawAGkLY4G2uquVSDj3uV4GAh 👾 Pass (1 warning) x86_64@clang-nightly 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw31cBiEQLj5jNyVuBuB7gC7 👹 Fail (4 errors) arm64@clang-nightly 👹 Fail: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw6wxlpa2mjvSd1VG2m9xayA 👹 Fail (308 errors) powerpc@clang-nightly 👹 Fail: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIawAFon7E369xcrFfVukYdMYj 👹 Fail (303 errors) powerpc@clang-nightly 👹 Fail: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw86L6PuXS5UT8s8gidCwo00 👹 Fail (0 errors) powerpc@gcc-11 👹 Fail: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw60V3G9tNDSH8but34eWiov 👹 Fail (0 errors) powerpc@gcc-11 👹 Fail: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIaw64tBjfDTtk8Pf9e8ecOJCt 👹 Fail (3 errors) sparc@gcc-11 👹 Fail: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
```

The [plan details](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uIavoQmdr5lRisqJWSZfL2iRCx)
are available at a non-guessable URL, along with links to each individual builds.
