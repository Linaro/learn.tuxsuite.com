# Architecture validation

A plan is a combination of builds and tests.

For each build, it's possible to define tests that would be triggered when the
build is successful. This dependency is declared at submission time and later
handled directly by the server.

## Plan configuration

This plan will build the given linux kernel for armv7 with every available
toolchains. Each build will then be tested with every available LTP test
suites.

```yaml
version: 1
name: armv7 validation
description: Build and test linux kernel for armv7
jobs:
- name: armv7
  builds:
    - {toolchain: gcc-8, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: gcc-9, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: gcc-10, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: gcc-11, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: clang-10, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: clang-11, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: clang-12, target_arch: arm, kconfig: multi_v7_defconfig}
    - {toolchain: clang-nightly, target_arch: arm, kconfig: multi_v7_defconfig}
  tests:
    - {device: qemu-armv7, tests: [ltp-fcntl-locktests]}
    - {device: qemu-armv7, tests: [ltp-fs_bind]}
    - {device: qemu-armv7, tests: [ltp-fsx]}
    - {device: qemu-armv7, tests: [ltp-nptl]}
    - {device: qemu-armv7, tests: [ltp-smoke]}
```

## Command line

Submit the multiple build request using the tuxsuite command line interface. This will
wait for all the build and test to complete before returning by default.

```shell
tuxsuite plan \
    --git-repo "https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git" \
    --git-ref "master" \
    plan.yaml
```

## Outputs

The output will look like:

```shell
Running Linux Kernel plan 'armv7 validation': 'Build and test linux kernel for armv7'
Plan https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uIYAFbx6NuJIxgMpqQO5TPVdC4

uid: 1uIYAFbx6NuJIxgMpqQO5TPVdC4
⚙️ Provisioning: arm (multi_v7_defconfig) with gcc-8 @ https://builds.tuxbuild.com/1uIYANv7cFmoleO1OMNxFM7lYLw/
⚙️ Provisioning: arm (multi_v7_defconfig) with gcc-9 @ https://builds.tuxbuild.com/1uIYAMypPwZ9igpbummWoR6K3jn/
⚙️ Provisioning: arm (multi_v7_defconfig) with gcc-10 @ https://builds.tuxbuild.com/1uIYAORkhKZLuF4sR1ZOvdvTAaW/
⚙️ Provisioning: arm (multi_v7_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1uIYANWT1hOf2sdgqQq427xaPCg/
⚙️ Provisioning: arm (multi_v7_defconfig) with clang-10 @ https://builds.tuxbuild.com/1uIYANi4eKJraui4VgCW6eWVy8n/
⚙️ Provisioning: arm (multi_v7_defconfig) with clang-11 @ https://builds.tuxbuild.com/1uIYAY9jevfusrrtxSxpfi7woQz/
⚙️ Provisioning: arm (multi_v7_defconfig) with clang-12 @ https://builds.tuxbuild.com/1uIYAWgjclijr8R2qE0p3OAZCAF/
⚙️ Provisioning: arm (multi_v7_defconfig) with clang-nightly @ https://builds.tuxbuild.com/1uIYARKuBxug2phc1jJUN6TayFG/
⏳ Waiting: [boot, ltp-fcntl-locktests] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uIYAXgSEWGCQc9RQYVQuP17Np0
⏳ Waiting: [boot, ltp-fs_bind] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uIYAVgZMewoJN4D7jYl9d3bRAh
⏳ Waiting: [boot, ltp-fsx] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uIYAdhbLAkvmyFhGAEttfaHino
⏳ Waiting: [boot, ltp-nptl] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uIYAfdx3aSTpLJrCrzg3eXMske
⏳ Waiting: [boot, ltp-smoke] qemu-armv7 @ https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/tests/1uIYAdERuLdHrfTlAnacjQb5FIe
[...]
🚀 Running: a96bfed64c89 ("Merge tag 'for-linus' of git://git.armlinux.org.uk/~rmk/linux-arm") arm (multi_v7_defconfig) with gcc-8 @ https://builds.tuxbuild.com/1uIYANv7cFmoleO1OMNxFM7lYLw/
🚀 Running: a96bfed64c89 ("Merge tag 'for-linus' of git://git.armlinux.org.uk/~rmk/linux-arm") arm (multi_v7_defconfig) with gcc-9 @ https://builds.tuxbuild.com/1uIYAMypPwZ9igpbummWoR6K3jn/
🚀 Running: a96bfed64c89 ("Merge tag 'for-linus' of git://git.armlinux.org.uk/~rmk/linux-arm") arm (multi_v7_defconfig) with gcc-10 @ https://builds.tuxbuild.com/1uIYAORkhKZLuF4sR1ZOvdvTAaW/
🚀 Running: a96bfed64c89 ("Merge tag 'for-linus' of git://git.armlinux.org.uk/~rmk/linux-arm") arm (multi_v7_defconfig) with clang-10 @ https://builds.tuxbuild.com/1uIYANi4eKJraui4VgCW6eWVy8n/
🚀 Running: a96bfed64c89 ("Merge tag 'for-linus' of git://git.armlinux.org.uk/~rmk/linux-arm") arm (multi_v7_defconfig) with gcc-11 @ https://builds.tuxbuild.com/1uIYANWT1hOf2sdgqQq427xaPCg/
🚀 Running: a96bfed64c89 ("Merge tag 'for-linus' of git://git.armlinux.org.uk/~rmk/linux-arm") arm (multi_v7_defconfig) with clang-11 @ https://builds.tuxbuild.com/1uIYAY9jevfusrrtxSxpfi7woQz/
🚀 Running: a96bfed64c89 ("Merge tag 'for-linus' of git://git.armlinux.org.uk/~rmk/linux-arm") arm (multi_v7_defconfig) with clang-12 @ https://builds.tuxbuild.com/1uIYAWgjclijr8R2qE0p3OAZCAF/
🚀 Running: a96bfed64c89 ("Merge tag 'for-linus' of git://git.armlinux.org.uk/~rmk/linux-arm") arm (multi_v7_defconfig) with clang-nightly @ https://builds.tuxbuild.com/1uIYARKuBxug2phc1jJUN6TayFG/
[...]
Summary: https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uIYAFbx6NuJIxgMpqQO5TPVdC4
1uIYAORkhKZLuF4sR1ZOvdvTAaW 🎉 Pass arm@gcc-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIYANWT1hOf2sdgqQq427xaPCg 🎉 Pass arm@gcc-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIYANv7cFmoleO1OMNxFM7lYLw 🎉 Pass arm@gcc-8 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIYAMypPwZ9igpbummWoR6K3jn 🎉 Pass arm@gcc-9 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIYANi4eKJraui4VgCW6eWVy8n 👾 Pass (1 warning) arm@clang-10 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIYAY9jevfusrrtxSxpfi7woQz 👾 Pass (1 warning) arm@clang-11 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIYAWgjclijr8R2qE0p3OAZCAF 👾 Pass (1 warning) arm@clang-12 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
1uIYARKuBxug2phc1jJUN6TayFG 👾 Pass (1 warning) arm@clang-nightly 🎉 Pass: ltp-fcntl-locktests,ltp-fs_bind,ltp-fsx,ltp-nptl,ltp-smoke
```

The [plan details](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/remi/plans/1uIYAFbx6NuJIxgMpqQO5TPVdC4)
are available at a non-guessable URL, along with links to each individual builds.
