# Open Embedded plan 📜

A plan is a of combination of builds and tests, but for OE we currently support only builds.
We define our combinations of builds in a yaml file consisting of bake build or builds or both.

!!! Note
    For Reference of our yaml file containing combinations of builds, we will be referring it here as plan file

## Kinds of build supported by OE
* Android builds ![Android Builds](../../../assets/and.png){: style="height:15px;width:15px"}
- Kas builds 🚀
- Open embedded builds ![Open embedded builds](../../../assets/oed.svg){: style="height:20px;width:25px"}
- Openbmc builds ![Openbmc builds](../../../assets/openbmc.png){: style="height:25px;width:28px"}

## Plan configuration 📄

This plan file consists of all available kinds of builds along with their respective definitions.
We will look at their definitions in detail later in this doc.

#### plan.yaml

```yaml
version: 1
name: OEBUILD Examples
description: Build and test TS and LRP
jobs:
- name: Kas Builds
  bakes:
    - sources:
        kas:
          url: "https://git.codelinaro.org/linaro/dependable-boot/meta-ts.git"
          yaml: "ci/qemuarm64-secureboot.yml"
    - sources:
        kas:
          url: "https://git.codelinaro.org/linaro/dependable-boot/meta-ts.git"
          yaml: "ci/rockpi4b.yml"

- name: OE (Repo) builds
  bakes:
    - container: "ubuntu-20.04"
      distro: "rpb"
      envsetup: "setup-environment"
      machine: "ledge-multi-armv8"
      sources:
        repo:
          branch: "master"
          manifest: "default.xml"
          url: "https://github.com/Linaro/ledge-oe-manifest.git"
      target: "ledge-gateway"
    - container: "ubuntu-20.04"
      distro: "rpb"
      envsetup: "setup-environment"
      machine: "ledge-multi-armv8"
      sources:
        repo:
          branch: "master"
          manifest: "default.xml"
          url: "https://github.com/Linaro/ledge-oe-manifest.git"
      target: "ledge-iot"

- name: OE (Git_trees) builds
  bake:
    sources:
      git_trees:
      - url: http://git.yoctoproject.org/git/poky
        branch: honister
      - url: https://github.com/ndechesne/meta-qcom
        branch: honister
    container: ubuntu-20.04
    envsetup: poky/oe-init-build-env
    distro: poky
    machine: dragonboard-845c
    target: core-image-minimal
    bblayers_conf:
    - BBLAYERS += "../meta-qcom/"
    artifacts:
    - "$DEPLOY_DIR"
    environment: {}

- name: Android builds
  bake:
    artifacts: []
    bblayers_conf: []
    container: ubuntu-20.04
    distro:
    environment: {}
    envsetup:
    local_conf: []
    machine:
    name: ''
    sources:
      android:
        branch: common-android-mainline
        build_config: common/build.config.gki.aarch64
        manifest: default.xml
        url: https://android.googlesource.com/kernel/manifest
    targets: null

- name: Openbmc builds
  bake:
    artifacts: []
    bblayers_conf: []
    container: ubuntu-18.04
    distro: openbmc-romulus
    environment: {}
    envsetup: setup
    local_conf: []
    machine: romulus
    name: ''
    sources:
      git_trees:
        - branch: master
          url: https://github.com/openbmc/openbmc
    targets:
      - obmc-phosphor-image

```

!!! Warning "Usage"
    Bitbake support is experimental and is in development phase so it may break from time to time
## Command line 🧑🏻‍💻

Submit the multiple build request of the plan file using the [tuxsuite](https://docs.tuxsuite.com/) command line interface. This will
wait for all the build to complete before returning by default.
Tuxsuite cli will show various details of the builds like plan summary, various phases of ongoing builds along with its details and finally result after builds finishes.

```shell
tuxsuite plan plan.yaml
```

### Phases of builds are:
⚙️ Provisioning <br />
🚀 Running <br />
🎉 Pass | 👹 Fail | 💀 Infrastructure Error

``` mermaid
graph LR
  A[Provisioning] --> B{Running};
  B --->C[Pass]
  B ---> D[Fail];
  B --->E[Infrastructure Error]
```




## Outputs 🧾


The tuxsuite cli output will look like:

```shell

*** WARNING: BITBAKE SUPPORT IS EXPERIMENTAL ***
Running Bake plan 'OEBUILD Examples': 'Build and test TS and LRP'
Plan https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/alok/plans/2F57NDnDKr1pMA3L5rsA2U2k3e0

uid: 2F57NDnDKr1pMA3L5rsA2U2k3e0
⚙️  Provisioning: {'kas': {'url': 'https://git.codelinaro.org/linaro/dependable-boot/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NI6t0EzVJfabyZghp9PT02G/
⚙️  Provisioning: {'kas': {'url': 'https://git.codelinaro.org/linaro/dependable-boot/meta-ts.git', 'yaml': 'ci/rockpi4b.yml'}} with None None for None: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NKGCaWdkZdNXw9SzOMq1vn2/
⚙️  Provisioning: {'repo': {'branch': 'master', 'manifest': 'default.xml', 'url': 'https://github.com/Linaro/ledge-oe-manifest.git'}} with rpb ledge-multi-armv8 for ['ledge-gateway']: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NHeh3MRJwfpinRnqW8kTqrQ/
⚙️  Provisioning: {'repo': {'branch': 'master', 'manifest': 'default.xml', 'url': 'https://github.com/Linaro/ledge-oe-manifest.git'}} with rpb ledge-multi-armv8 for ['ledge-iot']: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NLeKOaWfBtpT7AnFYVOBYqA/
⚙️  Provisioning: {'git_trees': [{'url': 'http://git.yoctoproject.org/git/poky', 'branch': 'honister'}, {'url': 'https://github.com/ndechesne/meta-qcom', 'branch': 'honister'}]} with poky dragonboard-845c for ['core-image-minimal']: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NHqiqzZQkWGpk2Gt3gX1PUy/
⚙️  Provisioning: {'android': {'branch': 'common-android-mainline', 'build_config': 'common/build.config.gki.aarch64', 'manifest': 'default.xml', 'url': 'https://android.googlesource.com/kernel/manifest'}} with None None for None: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NN3arYXsREXXHrzfQbcGBoU/
🚀 Running: {'repo': {'branch': 'master', 'manifest': 'default.xml', 'url': 'https://github.com/Linaro/ledge-oe-manifest.git'}} with rpb ledge-multi-armv8 for ['ledge-gateway']: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NHeh3MRJwfpinRnqW8kTqrQ/
🚀 Running: {'repo': {'branch': 'master', 'manifest': 'default.xml', 'url': 'https://github.com/Linaro/ledge-oe-manifest.git'}} with rpb ledge-multi-armv8 for ['ledge-iot']: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NLeKOaWfBtpT7AnFYVOBYqA/
🚀 Running: {'git_trees': [{'url': 'http://git.yoctoproject.org/git/poky', 'branch': 'honister'}, {'url': 'https://github.com/ndechesne/meta-qcom', 'branch': 'honister'}]} with poky dragonboard-845c for ['core-image-minimal']: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NHqiqzZQkWGpk2Gt3gX1PUy/
🚀 Running: {'android': {'branch': 'common-android-mainline', 'build_config': 'common/build.config.gki.aarch64', 'manifest': 'default.xml', 'url': 'https://android.googlesource.com/kernel/manifest'}} with None None for None: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NN3arYXsREXXHrzfQbcGBoU/
🚀 Running: {'kas': {'url': 'https://git.codelinaro.org/linaro/dependable-boot/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NI6t0EzVJfabyZghp9PT02G/
🚀 Running: {'kas': {'url': 'https://git.codelinaro.org/linaro/dependable-boot/meta-ts.git', 'yaml': 'ci/rockpi4b.yml'}} with None None for None: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NKGCaWdkZdNXw9SzOMq1vn2/
🎉 Pass: {'kas': {'url': 'https://git.codelinaro.org/linaro/dependable-boot/meta-ts.git', 'yaml': 'ci/qemuarm64-secureboot.yml'}} with None None for None: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NI6t0EzVJfabyZghp9PT02G/
🎉 Pass: {'kas': {'url': 'https://git.codelinaro.org/linaro/dependable-boot/meta-ts.git', 'yaml': 'ci/rockpi4b.yml'}} with None None for None: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NKGCaWdkZdNXw9SzOMq1vn2/
👹 Fail: {'git_trees': [{'url': 'http://git.yoctoproject.org/git/poky', 'branch': 'honister'}, {'url': 'https://github.com/ndechesne/meta-qcom', 'branch': 'honister'}]} with poky dragonboard-845c for ['core-image-minimal']: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NHqiqzZQkWGpk2Gt3gX1PUy/
🎉 Pass: {'android': {'branch': 'common-android-mainline', 'build_config': 'common/build.config.gki.aarch64', 'manifest': 'default.xml', 'url': 'https://android.googlesource.com/kernel/manifest'}} with None None for None: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NN3arYXsREXXHrzfQbcGBoU/
👹 Fail: {'repo': {'branch': 'master', 'manifest': 'default.xml', 'url': 'https://github.com/Linaro/ledge-oe-manifest.git'}} with rpb ledge-multi-armv8 for ['ledge-gateway']: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NHeh3MRJwfpinRnqW8kTqrQ/
👹 Fail: {'repo': {'branch': 'master', 'manifest': 'default.xml', 'url': 'https://github.com/Linaro/ledge-oe-manifest.git'}} with rpb ledge-multi-armv8 for ['ledge-iot']: https://storage.tuxsuite.com/public/tuxsuite/alok/oebuilds/2F57NLeKOaWfBtpT7AnFYVOBYqA/

Summary: https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/alok/plans/2F57NDnDKr1pMA3L5rsA2U2k3e0
2F57NI6t0EzVJfabyZghp9PT02G 🎉 Pass with container: ubuntu-20.04 machine: None targets: None
2F57NKGCaWdkZdNXw9SzOMq1vn2 🎉 Pass with container: ubuntu-20.04 machine: None targets: None
2F57NN3arYXsREXXHrzfQbcGBoU 🎉 Pass with container: ubuntu-20.04 machine: None targets: None
2F57NHeh3MRJwfpinRnqW8kTqrQ 👹 Fail (0 errors) with container: ubuntu-20.04 machine: ledge-multi-armv8 targets: ['ledge-gateway']
2F57NLeKOaWfBtpT7AnFYVOBYqA 👹 Fail (0 errors) with container: ubuntu-20.04 machine: ledge-multi-armv8 targets: ['ledge-iot']
2F57NHqiqzZQkWGpk2Gt3gX1PUy 👹 Fail (0 errors) with container: ubuntu-20.04 machine: dragonboard-845c targets: ['core-image-minimal']

```

The [plan details](https://tuxapi.tuxsuite.com/v1/groups/tuxsuite/projects/alok/plans/2F57NDnDKr1pMA3L5rsA2U2k3e0)
are available at a non-guessable URL, along with links to each individual builds. Here you will find extra information about respective builds,
the artefacts that they may have generated and log files.
