# Plan file definition

## Version

Currently, tuxsuite only supports version 1.

```yaml
version: 1
```

## Name and description

The name and description are are only used if they are not defined on the command line, although they are optional field in plan file.

```yaml
name: plan name
description: plan description
```

They can be overridden on the command line:

```
tuxsuite plan --name "another name" --description "another description"
```

## Jobs

Most of the configuration takes place in the `jobs` section.

This section is a list of dictionary with one or two keys each.

Allowed combinations are:

* bake
* bakes
* bake and bakes both
### Bake

A bake is defined by a dictionary:

```yaml
- bake:
    build definition
```
### Bakes

When specifying multiple oebuilds, the job should be a list of dictionaries like:

```yaml
- bakes:
  - build definition 1
  - build definition 2
  - build definition 3
  [....]
```
#### Build definitions

TuxOE uses build-definition to describes the build:

* the layers that should be downloaded
* `local.conf` and `bblayers.conf` to use
* machine type
* distro targets
* ...

TuxOE can downloads the layers using either git protocols or the repo tool.

### Format 📑

=== "**Android Definition**"
    - `container`: The container used by Docker to do the build. We currently support:
         - [x] ubuntu-16.04
         - [x] ubuntu-18.04
         - [x] ubuntu-20.04
         - [x] ubuntu-22.04
         - [x] centos-8
         - [x] debian-bullseye
         - [x] debian-buster
         - [x] debian-stretch
         - [x] fedora-33
         - [x] fedora-34
         - [x] opensuse-leap-15.1
         - [x] opensuse-leap-15.2
    - `name`: (optional) Name of your choice
    - `sources`:
        - `android`: Android repo source, its branch, manifest, build-config and its url to be provided under `android` field
    !!! Tip "Note"
        To build android 14.5.15 and common-android-mainline branch, field `bazel` has to be passed in sources. It accepts boolean value and defaults to false.

=== "**Kas Definition**"
    - `container`: The container used by Docker to do the build. Supported containers are same as in android definition
    - `sources`:
        - `kas`: (a dictionary): Each dictionary will have **url**, **branch**, **yaml** and optionally **ref**. No other fields are allowed when you use kas.


=== "**OE Definition**"

    The build definition can include the following fields:

    - `sources`: (dictionary with a single item): Could be one **git_trees**, **repo**
        * `git_trees`: (list of dictionaries): Each dictionary will have **url**, **branch** and optionally **ref**
        * `repo`: (a dictionary): Should have **branch**, **manifest** and **url**, describing where the manifests are hosted along with the branch and manifest file to be used in the build.
    * `distro`: The distro variable passed to OE build
    * `envsetup`: Path to the script, relative to the source directory, that needs to be sourced to setup bitbake build environment
    * `extraconfigs` (list of strings): each entry corresponds to some extra configs that will be copied to conf folder and will be using while building the target. It is used only in case of build definition containing sources as **repo** or **git_trees**
    * `machine`: The machine variable passed to OE build
    * `target`: Target passed to the bitbake command
    * `container`: The container used by Docker to do the build. Supported containers are same as in android definition
    * `local_conf`: (list of strings): Each entry corresponds to a line in local.conf file. The list of string is converted to `local.conf` file
    * `bblayers_conf:` (list of strings): Each entry corresponds to a line in bblayers.conf file. The list of string is converted to `bblayers.conf` file
    * `environment`: (dictionary of environment variables): They are set before calling bitbake

    !!! Note
        In OE build definition sources, you can either provide git_trees or repo as sources but not both

=== "**OpenBmc**"

    Build definition for Openbmc is almost similar to `OE Definition` except below changes:

    - Distro is prefixed with `openbmc`
    - sources consists of `git_trees`
