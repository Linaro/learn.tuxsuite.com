# TuxSuite Introduction

TuxSuite is an on demand API for building and testing massive quantities of
Linux kernels in parallel.

It is used at scale in production by [LKFT](https://lkft.linaro.org/) and
[ClangBuiltLinux](https://clangbuiltlinux.github.io/) as well as many
individual Linux kernel engineers.

You can have access to the API using the tuxsuite command line tool.
