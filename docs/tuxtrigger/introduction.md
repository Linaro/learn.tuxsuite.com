# About TuxTrigger


TuxTrigger, by [Linaro](https://www.linaro.org/), is a command line tool for controlling changes in repositories.  

[TuxTrigger](https://linaro.gitlab.io/tuxtrigger) allows to automatically track a set of git repositories and branches. When a change occurs, TuxTrigger will build, test and track the results using Tuxsuite and [SQUAD](https://squad.readthedocs.io/).

In order to use Tuxtrigger you first need an tuxsuite and SQUAD account.

- [Configuring tuxsuite](../introduction/configuring.md)
- Configuring SQUAD
    1. create group -> `/settings/projects`
    2. create project for each tracked repository
    3. get api token -> `/settings/api-token`



