
# Running TuxTrigger


## Remotely (Gitlab)

TuxTrigger can be run remotely as scheduled gitlab pipeline.

!!!note
    Remember to set `SQUAD_HOST`, `SQUAD_TOKEN` and `TUXSUITE_TOKEN` variables in
    gitlab CI/CD settings!

Example gitlab.ci file running in `yaml` mode

```yaml
variables:
  CACHE_FALLBACK_KEY: main-$CACHE_KEY_INDEX-protected

building:
  stage: build
  image: registry.gitlab.com/linaro/tuxtrigger/tuxtrigger
  variables:
    CACHE_DIR: "$CI_PROJECT_DIR/.cache/"
    TUXSUITE_TOKEN: $TUXSUITE_TOKEN
    SQUAD_TOKEN: $SQUAD_TOKEN
    SQUAD_HOST: $SQUAD_HOST
  script:
    - python3 -m tuxtrigger share/config_demo.yaml --plan share/plans \
	--output share/output_file.yaml --log-level=INFO --log-file share/log.txt
  cache:
    key: $CI_COMMIT_REF_SLUG
    paths:
    - ${CI_PROJECT_DIR}/share
  artifacts:
    paths:
    - ${CI_PROJECT_DIR}/share/log.txt
    - ${CI_PROJECT_DIR}/share/output_file.yaml
```

## Locally 

You can run TuxTrigger locally on your computer

```shell
tuxtrigger share/config_demo.yaml \
    --plan share/plans \
    --output share/output_file.yaml \
    --log-level=INFO \
    --log-file share/log.txt

```

To run TuxTrigger regularly a CRON job can be scheduled.

