# TuxTrigger Configuration

!!!note
    If you don't have an Tuxsuite and/or SQUAD account read [this](../introduction)


## Environment Variables

In order to run TuxTrigger you need to declare the following environment
variables:

1. `SQUAD_HOST` - SQUAD url address
2. `SQUAD_TOKEN` - SQUAD API token
3. `TUXSUITE_TOKEN` - tuxsuite authentication token


## Configuration file

To make TuxTrigger work you have to provide configuration .yaml file with
declared SQUAD details and repositories data (url to tracked repository,
selected branches and plan.yaml file for TuxSuite Plan).

Example of basic config.yaml

```yaml
repositories:
- url: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
  squad_group: your_squad_group_name
  branches:
    - name: master
      squad_project: tuxtrigger-main
      plan: stable.yaml
    - name: v5.19-rc6
      squad_project: tuxtrigger-torvalds-main
      plan: stable_next.yaml
      lava_test_plans_project: lkft
      lab: https://lkft.validation.linaro.org
```

Tuxtrigger enables dynamically generated branch list in config file:

- 'regex' - match branch names in selected repository
- 'default_plan' - plan to be assigned for matched branches
- 'squad_project_prefix' - prefix added to 'squad_project' value in matched branches
- 'default_squad_project' - Use this squad_project value in matched branches
- 'default_lava_test_plans_project' - Use to set value in matched branches
- 'default_lab' - Use to set lab for all matched branches
- 'lava_test_plans_project' - Use to say what project in lava-test-plans to use
- 'lab' - Use to say what LAVA lab to submit jobs to

```yaml
repositories:
- url: https://git.kernel.org/pub/scm/linux/kernel/git/arm64/linux.git
  squad_group: your_squad_group_name
  regex: for-next/*
  default_plan: stable.yaml
  squad_project_prefix: generator
  default_lava_test_plans_project: lkft
  default_lab: https://lkft.validation.linaro.org
  branches:
  - name: for-next/acpi # hardcoded values won't be overwritten
    squad_project: generator-linux-for-next
    plan: stable_next.yaml
```

Tuxtrigger enables SQAUD project configuration. By setting values in config file you are able to 
create or update squad project.
- in ```config``` section you are able to specify one or more options from listed below

```yaml
config:
  plugins: linux_log_parser,ltp
  wait_before_notification_timeout: 600
  notification_timeout: 28800
  force_finishing_builds_on_timeout: False
  important_metadata_keys: build-url,git_ref,git_describe,git_repo,kernel_version
  thresholds: build/*-warnings
  data_retention: 0
repositories:
- url: https://git.kernel.org/pub/scm/linux/kernel/git/arm64/linux.git
  squad_group: your_squad_group_name
  regex: for-next/*
  default_plan: stable.yaml
  squad_project_prefix: generator
  default_lava_test_plans_project: lkft
  default_lab: https://lkft.validation.linaro.org
  branches:
  - name: for-next/acpi # hardcoded values won't be overwritten
    squad_project: generator-linux-for-next
    plan: stable_next.yaml
```

To check results of dynamically generated config use "--generate-config" argument. 
Tuxtrigger will perform 'dry-run' and prompt generated config.

```shell
tuxtrigger /path/to/config.yaml --generate-config
```

If ```squad_project``` is not declared in configuration file, tuxtrigger will create
that value automatically from last url segment and branch name

```yaml
repositories:
- url: https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
  squad_group: your_squad_group_name
  branches:
    - name: v5.19
      squad_project: 
      plan: stable.yaml

# squad_project will be declared as: linux-v5.19
``` 

## Run mode yaml/squad

TuxTrigger checks out repositories by comparing fingerprint from
[git.kernel.org manifest](https://git.kernel.org/manifest.js.gz) file and/or
git sha value.

## From version 0.8.0

Tuxtrigger is storing fingerprint and/or git sha values ONLY in output yaml file.

!!! note
	- ```sha-compare``` subcommand is no longer available


## Version up to 0.7.0

There are two options where those values are stored in order to compare.

1. SQUAD - recommended
sha-compare argument is set by default to `squad`
fingerprint and git sha are stored in squad_project metadata

```shell
tuxtrigger path/to/config.yaml --sha-compare=squad
```

2. YAML
fingerprint and git-sha values are stored in .yaml file created by tuxtrigger
in `share` folder

```shell
tuxtrigger path/to/config.yaml --sha-compare=yaml
```

## Plan file(s)

For sending plan to TuxSuite you must provide relevant plan (and include that
in the configuration file)

Example of a plan file

```yaml
version: 1
name: stable_plan
description: stable_plan
jobs:
- tests:
    - {device: qemu-x86_64, tests: [ltp-smoke]}
```

For further information about plans please Visit the [plan tutorial](../../cli/plan/kernel/arch)


By default TuxTrigger takes plans from `share/plans` folder Custom path can be
set by argument `--plan <path>`

```shell
tuxtrigger path/to/config.yaml --plan=path/to/plan/folder
```

## Additional configuration


### Submit mode

By using ```--submit``` argument you are able to change behavior of tuxtrigger

- ```never``` - tuxtrigger will not submit build, plan to tuxsuite and will not send information to SQUAD
- ```change``` - (default value) - tuxtrigger will submit build, plan to tuxsuite and will send information to SQUAD only when change occurred in tracked repository
- ```always``` - tuxtrigger will submit build, plan to tuxsuite and will send information to SQUAD for every tracked branch from config file. Regardless of changes.

```shell 
tuxtrigger path/to/config.yaml --submit=always
```

### Pre submit script

When tracked repository has changed tuxtrigger can invoke your custom script. 
By passing ```--pre-submit``` argument you are able to define the path to script, which receives three values from tuxtrigger

1. repository url
2. branch name
3. SHA value

```shell 
tuxtrigger path/to/config.yaml --pre-submit path/to/script.sh
```
### Disabling Tuxplan submit

Tuxtrigger can be used without sending a plan file to tuxsuite, by adding an argument ```--disable-plan```
With that option tuxtrigger will check repositories for changes and can invoke the script if repo has changed.
After script execution tuxtrigger will save new sha/fingerprint data to output file.
(even when tuxplan option is disabled tuxtrigger will still invoke tuxsuite build for receiving ‘git_describe’ value).

```shell 
tuxtrigger path/to/config.yaml --disable-plan
```

### Disabling SQUAD submit

Tuxtrigger can be used without sending/using SQUAD ```--disable-squad```
With that option tuxtrigger will check repositories for changes and invoke tuxbuild and tuxplan when repo has changed. 
It can also invoke the script when this command is specified.
After script execution tuxtrigger will save new sha/fingerprint data to output file.

```shell 
tuxtrigger path/to/config.yaml --disable-squad
```

### Generate config

Argument ```--generate-config``` invoke tuxtrigger dry-run to prompt the result of generating configuration from config.yaml file. (also, you can check if regex value set in config file matches any branches from tracked repository)

```shell 
tuxtrigger path/to/config.yaml --generate-config
```

### Json output

```--json-out``` argument ables setting the path for json output file generated when plan is successfully submitted to tuxsuite

```shell
tuxtrigger path/to/config.yaml --json-out path/to/file.json
```

### output file

if yaml run mode is selected TuxTrigger creates output file (by default set as
`share/gitsha.yaml`) you are able to set custom value by using `--output`
argument

```shell
tuxtrigger path/to/config.yaml --output=path/to/output.yaml
```

### Callback url

It is possible to set callback-url inside tuxtrigger with ```--callback-url``` command

```shell
tuxtrigger path/to/config.yaml --callback-url https://url
```

### Callback headers

To specify headers passed to callback-url you can set ```--callback-headers``` subcommand formatted as 'header':'value' pairs
```shell
tuxtrigger path/to/config.yaml --callback-headers XYZ
```

### log file

The default log file name is `log.txt` and can be changed with `--log-file`:

```shell
tuxtrigger path/to/config.yaml --log-file=path/to/log.txt
```

### log level {DEBUG, INFO, WARN, ERROR}

By using log level (`--log-level`) argument you are able to set log level
available options: DEBUG, INFO, WARN, ERROR. By default level is set to `INFO`

```shell
tuxtrigger path/to/config.yaml --log-level=WARN
```
