## Installing from PyPI <small>recommended</small>

Tuxtrigger is published as Python package and can be installed with pip

!!! note
    TuxTrigger requires Python 3.6 or newer.

To install TuxTrigger on your system globally:

```shell
sudo pip3 install -U TuxTrigger
```

To install TuxTrigger to your home directory at ~/.local/bin:

```shell
pip3 install -U --user TuxTrigger
```

To upgrade TuxTrigger to the latest version, run the same command you ran to
install it.

