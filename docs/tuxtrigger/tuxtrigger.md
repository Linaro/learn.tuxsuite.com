# Tuxtrigger on the cloud

Tuxsuite provides a way to automatically track git repositories by running tuxtrigger on the cloud.
This way, tuxtrigger does not need to be run manually or as a CI job frequently to build, test and
track repositories in case of changes. Tuxtrigger, which runs on the cloud every ten minutes, does the job for us.

The following are needed to track git repositories for changes:

* Config
* Plans

Tuxtrigger running on the cloud will use these configuration and plan files to build, test and track git repositories.

To learn more about how tuxsuite cli can be used to upload these files, click [here](https://docs.tuxsuite.com/trigger/trigger/){target=_blank}.
