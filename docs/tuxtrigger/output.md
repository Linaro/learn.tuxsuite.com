
# Output yaml file

## From version 0.8.0

Tuxtrigger is storing fingerprint and/or git sha values ONLY in output yaml file.

!!! note
	- ```sha-compare``` subcommand is no longer available


## Version up to 0.7.0

!!!note
    output file is only created when TuxTrigger is running in 'yaml' mode. See [configuration](../configuration) for more details.

By default output file will be created in share/ folder.

example:

```yaml
https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git:
  fingerprint: 41773c7cfe09d1ab749037b98e38b9acf0fc14b5
  master:
    ref: refs/heads/master
    sha: c40e8341e3b3bb27e3a65b06b5b454626234c4f0
  v5.19-rc6:
    ref: refs/tags/v5.19-rc6
    sha: 8fcdc0ea4cc7ea79a1837f5040f5656f9be98f9f
```
