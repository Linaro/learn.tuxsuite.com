# Efficient Automated Builds for Private Repos with TuxSuite

## Introduction

This guides you through the process of setting up TuxSuite to build from a private tree hosted on GitHub or GitLab.

## Prerequisites

- TuxSuite account and CLI installed
- Private repository on GitHub or GitLab

## Generating Your Personal Access Token

### GitHub

1. Go to **Settings → Developer settings → Personal access tokens**.
2. Click on **Generate new token**.
3. Select the `repo` scope.
4. Click **Generate token**.

### GitLab

1. Go to **Settings → Access Tokens**.
2. Create a token with the `read_repository` scope.
3. Click **Create personal access token**.

## Uploading Your PAT to TuxSuite

Replace `<username>` with your (gitlab/github) username and `<your_PAT>` with the generated PAT.

```bash
# GitHub
tuxsuite keys add --type pat --domain github.com --username <username> --token <your_PAT>
# GitLab
tuxsuite keys add --type pat --domain gitlab.com --username <username> --token <your_PAT>
```

## Starting a Private Build

To initiate a private build with your private repository, execute the following command with the TuxSuite CLI:

```bash
tuxsuite plan --git-repo <your_private_repo_url> --git-ref <branch_or_tag> --private plan.yaml
```

Replace `<your_private_repo_url>` and `<branch_or_tag>` with the URL of your private repository and the specific branch or tag you wish to build.

## Accessing Private Artifacts

After your build is complete, you can access the private artifacts by authenticating with your TuxSuite credentials:

```bash
curl -L -H 'Authorization: token <your_tuxsuite_token>' \
'https://storage.tuxsuite.com/private/<your_project>/<build_id>/artifact' \
-o <destination_path>
```
Be sure to replace `<your_tuxsuite_token>`, `<your_project>`, `<build_id>`, and `<destination_path>` with your actual TuxSuite token, project name, build identifier, and the path where you wish to download the artifact, respectively.

## Conclusion

By meticulously following the steps outlined, you can harness TuxSuite to facilitate secure and efficient builds for your private repositories, ensuring your proprietary kernel development remains confidential and robust.
