# Tuxsuite Cloud Capabilities

While TuxSuite cloud supports plenty of features. Below are some of the prominent ones.

## Features

| Feature                                        | Native TuxSuite Execution         | TuxSuite Cloud |
|------------------------------------------------|:---------------------------------:|:--------------:|
| Test Sharding                                  | ✅                                | ✅             |
| Specify test Fragments in plans                | ✅                                | ✅             |
| Petition for new test workloads                | ✅                                | ✅             |
| Target tests on real remote-lab hardware       | ❌                                | ✅             |
| FVP test targets                               | ❌                                | ✅             |
| Reproducer fragments                           | ❌                                | ✅             |
| True Parallelization of builds and tests       | ❌                                | ✅             |
| Email summary/Report for a Plan                | ❌                                | ✅             |

## Get in Touch

For any inquiries or support, don't hesitate to [contact us](mailto:tuxsuite@linaro.org). We're here to help!
